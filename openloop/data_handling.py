import os, errno, datetime, pickle, glob

colDictPath = 'colDict.pkl'

class DataWriter():
    # Class for handling all of the data produced by the optimisations. Is
    # responsible for saving and loading of the data.

    def __init__(self, description):
        # Creates an instance of a DataHandler with a folder dedicated to
        # this so that all results can be saved.

        self.path = self.generatePath()
        self.saveDescription(description)

    def generatePath(self):
        # Generates a path where all results can be saved.

        path = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        self.makePath(path)
        return path

    def saveDescription(self, description):
        # Saves a description of the results being saved in this path.

        fileName = 'description.txt'
        fileName = self.path + '/' + fileName
        file = open(fileName, 'w')
        file.write(description)
        file.close()

    def saveResult(self, result):
        # Pickles a result and saves it as a unique file in path.

        fileName = datetime.datetime.now().strftime("%Y%m%d-%H%M%S%f")+'.pkl'
        fileName = self.path + '/' + fileName
        output = open(fileName, 'wb')
        pickle.dump(result, output)
        output.close()

    def saveColourDict(self, colourDict):
        # Pickles a curves-colour dictionary. There is only ever one of these
        # in a folder.

        fileName = self.path + '/' + colDictPath
        output = open(fileName, 'wb')
        pickle.dump(colourDict, output)
        output.close()

    def makePath(self, path):
        # If the directory doesn't exist, make it.

        try:
            os.makedirs(path)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

class DataReader():
    # Class for reading in the saved results.

    def __init__(self, path):
        # Creates a reader that will read all of the results in the path.

        self.path = path
        self.colDictPath = path + '/' + colDictPath
        self.results = []
        self.colDict = {}

    def readResults(self):
        # Reads all of the results in and appends them to the results list.

        for file in glob.glob("{}/*.pkl".format(self.path)):
            if file != self.colDictPath:
                pklFile = open(file, 'rb')
                self.results.append(pickle.load(pklFile))
                pklFile.close()
            print(file)

    def readColDict(self):
        # Reads the colour dictionary.

        for file in glob.glob(self.colDictPath):
            pklFile = open(file, 'rb')
            self.colDict = pickle.load(pklFile)
            pklFile.close()

