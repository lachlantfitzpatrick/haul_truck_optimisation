# This code is used to support the simulation of a dump truck as it moves up a
# slope. It is assumed that during this process there is no braking.

import math
import operator
import time


class Truck():
    # This class represents a dump truck. A dump truck is represented by its
    # position, velocity, acceleration, stored energy and power flow constraints.

    g = 9.807  # gravitational acceleration

    def __init__(self, slope, state=(0, 2, 70000000),
                 mass=570000, muRolling=0.02):
        # Creates a moving truck at the bottom of a slope with the amount
        # of stored energy according to a simulation of a CAT 795F-AC down a
        # slope of gradient 10deg on a rolling coefficient of 2%. The mass is
        # given in kilograms.
        # The state is the initial conditions of the truck. The first element
        # is the position along the slope (m), the second is the velocity along
        # the slope (m/s) and the final is the starting stored energy (J).
        self.state = state
        self.powerFlow = PowerFlow()
        self.mass = mass
        self.muRolling = muRolling
        self.slope = slope
        self.history = [(0,) + state + (0, 0, 0)]
        self.inputs = []
        self.time = 0
        self.energyUsed = (0, 0)

    def step(self, timeStep, powerIn):
        # Steps the simulation through from the current state to the next
        # state according to the input powers. The first element of powerIn is
        # the generator power and the second is the ERS power.

        # Apply the minimum allowed powers.
        powerIn = self.powerFlow.applyMin(powerIn)

        # start = time.time()
        # Apply the efficiencies to the powerIn to get the applied power.
        pT = self.powerFlow.powerOut(powerIn)

        # Calculate the rates of change of the state.
        # velocity
        delX1 = self.state[1]
        # acceleration
        # calculate forces: traction/applied, gravity, friction
        fT = self.calcTractionForce(pT)
        gradient = self.slope.getAngles(self.state[0])
        fG = self.mass * self.g * math.sin(gradient)
        fF = self.mass * self.g * self.muRolling * math.cos(gradient)
        fNet = self.calcNetForce(fT, fG, fF)
        delX2 = fNet / self.mass
        # ERS power
        delX3 = -powerIn[1] # Negate as this is the power leaving the ERS.
        delState = (delX1, delX2, delX3)

        # Perform the step to the next state
        stateStep = tuple([timeStep * x for x in delState])
        self.state = tuple(map(operator.add, self.state, stateStep))

        # Update the history.
        self.history.append(
            (self.history[-1][0] + timeStep,) + self.state + powerIn +
            (self.history[-1][6] + powerIn[0] * timeStep,))
        self.inputs.append([powerIn[0], powerIn[1]])
        self.energyUsed = (self.energyUsed[0]+powerIn[0]*timeStep,
                           self.energyUsed[1]+powerIn[1]*timeStep)
        self.time += timeStep
        # end = time.time()
        # print(end-start)

    def calcNetForce(self, fT, fG, fF):
        # Calculate the net force applied to the wheels after friction and
        # gravity have been applied to the traction/applied force.
        fNet = fT - fG
        # If the truck is stationary.
        if self.state[1] == 0:
            # Check net force direction and apply friction opposite.
            if fNet >= 0:
                fNet -= fF
                if fNet >= 0:
                    return fNet
                else:
                    return 0
            else:
                fNet += fF
                if fNet <= 0:
                    return fNet
                else:
                    return 0
        # If the truck moving forward friction is applied opposite.
        elif self.state[1] > 0:
            return fNet - fF
        # If the truck moving backward friction is applied opposite.
        else:
            return fNet + fF

    def calcTractionForce(self, powerApplied):
        # Given the power applied at the wheels, determines the force
        # that will be applied. This is a stepwise function according to
        # the speed of the truck.

        # Key info for saturation linearisation.
        m = 73509.9  # N s/m
        c = 1311000  # N

        # The saturation speed.
        satV = self.calcSatV(powerApplied, m, c)

        # Determine the force according to a piecewise function.
        if self.state[1] > satV:
            # Use the power/force curve.
            return powerApplied / self.state[1]
        else:
            # Use the saturated linearisation.
            return (c - self.state[1] * m)

    def calcSatV(self, powerApplied, m, c):
        # The velocity at which the drive motor becomes saturated. This is
        # the intercept between the linearisation and the normal power curve.
        return (c - math.sqrt(c ** 2 - 4 * m * powerApplied)) / (2 * m)

    def getGenEnergyUsed(self):
        # Gets amount of energy supplied by the generator.
        return self.energyUsed[0]

    def getERSenergyUsed(self):
        # Gets amount of ERS energy used by the truck.
        return self.energyUsed[1]

    def getPowerFlow(self):
        return self.powerFlow


class PowerFlow():
    # This class contains all of the information about how the power flows
    # for a given truck model.

    def __init__(self, effGenDC=0.95, effERSDC=0.9, effDCout=0.85,
                 maxERSpower=1390000, maxGenPower=2390000,
                 maxOutPower=2800000, minPower=0):
        # Creates a powerflow model.
        self.effGenDC = effGenDC
        self.effERSDC = effERSDC
        self.effDCout = effDCout
        self.maxERSpower = maxERSpower
        self.maxGenPower = maxGenPower
        self.maxOutPower = maxOutPower
        self.minPower = minPower

    def powerOut(self, powerIn):
        # Takes an input power from the generator and ERS and gives the power
        # applied to the wheels. The first element of powerIn is
        # the generator power and the second is the ERS power.

        # Choke both input powers to ensure they are below their allowed
        # maximums.
        powerIn = [powerIn[0], powerIn[1]]
        powerIn[0] = powerIn[0] if powerIn[0] < self.maxGenPower else \
            self.maxGenPower
        powerIn[1] = powerIn[1] if powerIn[1] < self.maxERSpower else \
            self.maxERSpower

        totOutP = self.effDCout * (self.effGenDC * powerIn[0] +
                                   self.effERSDC * powerIn[1])
        # Choke the power to below the maximum output.
        if totOutP > self.maxOutPower:
            # print('Maxed power.')
            return self.maxOutPower
        else:
            return totOutP

    def applyMin(self, powerIn):
        # Applies the minimum power input allowed

        if powerIn[0] < self.minPower:
            powerIn = (self.minPower, powerIn[1])
        if powerIn[1] < self.minPower:
            powerIn = (powerIn[0], self.minPower)

        return powerIn


class Slope():
    # This class represents a slope that a dump truck traverses.

    def __init__(self, gradients):
        # Creates a slope from the gradients. Gradients is a tuple of lists
        # with the first element as the distance s and the second value the
        # gradient that the slope has from that distance to the next angle
        # change. The distances should start from 0 and remain positive. The
        # gradients should be given in % (eg. for 10% gradient use 0.1).

        self.gradients = gradients
        self.angles = self.grad2Rad(gradients)

    def grad2Rad(self, gradients):
        # Converts the percentage gradients to angles.
        angles = ([], [])  # Initialise angles.
        for i in range(len(gradients[0])):
            angles[0].append(gradients[0][i])
            angles[1].append(math.atan(gradients[1][i]))
        return angles

    def getAngles(self, s):
        # Gets the gradient that corresponds to distance s along slope.

        # If there is only one element return it.
        if len(self.angles[0]) == 1:
            return self.angles[1][0]
        # Iterate over all the distances until the correct range is found.
        for i in range(len(self.angles[0]) - 1):
            if self.angles[0][i] <= s and s < self.angles[0][i + 1]:
                return self.angles[1][i]
        # If we get here s must lie above the final entry of angles hence
        # use the last entry.
        return self.angles[1][len(self.angles[0]) - 1]

    def getGradient(self, s):
        # Gets the gradient that corresponds to distance s along slope.

        # If there is only one element return it.
        if len(self.angles[0]) == 1:
            return self.gradients[1][0]
        # Iterate over all the distances until the correct range is found.
        for i in range(len(self.angles[0]) - 1):
            if self.angles[0][i] <= s and s < self.angles[0][i + 1]:
                return self.gradients[1][i]
        # If we get here s must lie above the final entry of angles hence
        # use the last entry.
        return self.gradients[1][len(self.gradients[0]) - 1]

    def getHeight(self, s):
        # Gets the height that the truck has travelled up based on the total
        # distance it has travelled.

        # If there is only one element use it.
        if len(self.angles[0]) == 1:
            return s * math.sin(self.angles[1][0])

        # Iterate through the angles until we reach the distance given.
        height = 0
        for i in range(len(self.angles[0]) - 1):
            # We only need to add this height and return as s is between this
            # distance i and next distance i+1
            if s < self.angles[0][i + 1]:
                return height + (s - self.angles[0][i]) * math.sin(
                    self.angles[1][i])
            else:
                height += (self.angles[0][i + 1] - self.angles[0][i]) * \
                          math.sin(self.angles[1][i])
        # If we get here s must lie above the final entry of angles hence
        # use the last entry.
        i = len(self.angles[0]) - 1
        return height + (s - self.angles[0][i]) * math.sin(self.angles[1][i])
