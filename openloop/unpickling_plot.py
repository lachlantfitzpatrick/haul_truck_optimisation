import optimisation
from data_handling import DataReader
import pickle, glob

def main():
    # Read a series of results and plot them.

    tryAgain = True
    path = ""
    while tryAgain:
        folderDict = {}
        i = 1
        for folder in glob.glob("*"):
            folderDict['{}'.format(i)] = folder
            print('{}: {}'.format(i, folder))
            i += 1
        selection = raw_input("Select the folder number: ")
        if selection in folderDict:
            path = folderDict[selection]
            tryAgain = False

    print(path)

    dataReader = DataReader(path)
    dataReader.readResults()
    dataReader.readColDict()

    results = dataReader.results
    colDict = dataReader.colDict

    optimisation.plotPareto(results, colDict)

if __name__ == '__main__':
    main()
