# This file is used to optimise the trajectory of the truck as it moves up
# the slope. It is not used to consider the control of the truck (merely the
# response).

plottingEnabled = True
saveRamEnabled = True

from optimisation_support import *
from data_handling import *
import math, gc, resource, datetime
from plot_support import get_cmap
from multiprocessing import Process, Manager
import multiprocessing
import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

patches = []
vehicleMass = 250000
initialState = (0, 7, 0)
simStep = 0.1
optStep = 5

def plotTruck(plotTruck, colour, dataName):
    # Plots a truck.

    # Get all of the data.
    t, h, v, genP, ERSp = [], [], [], [], []
    genE, ERSe = [], []
    for x in plotTruck.history:
        t.append(x[0])
        h.append(plotTruck.slope.getHeight(x[1]))
        v.append(x[2])
        genP.append(x[4])
        ERSp.append(x[5])
        genE.append(x[6])
        ERSe.append(x[3])

    # Plot velocity and height.
    fig1 = plt.figure(1)
    plt.subplot(211)
    plt.plot(t, v, color=colour)
    plt.ylabel('Velocity (m/s)')
    plt.subplot(212)
    plt.plot(t, h, color=colour, label='Height {}'.format(dataName))
    plt.ylabel('Height (m)')
    plt.xlabel('Time (m/s)')
    # patches.append(mpatches.Patch(color=colour, label='{}'.format(dataName)))
    # plt.legend(handles=patches, loc=4)
    # Plot the power use.
    fig2 = plt.figure(2)
    plt.subplot(211)
    plt.plot(t, genP, colour)
    plt.ylabel('Generator Power (W)')
    plt.subplot(212)
    plt.plot(t, ERSp, colour)
    plt.ylabel('ERS Power (W)')
    plt.xlabel('Time (s)')
    # plt.legend(handles=patches)
    # Plot the energy supplied.
    fig3 = plt.figure(3)
    plt.subplot(211)
    plt.plot(t, genE, colour)
    plt.ylabel('Generator Energy Supplied (J)')
    plt.subplot(212)
    plt.plot(t, ERSe, colour)
    plt.ylabel('ERS Energy Left (J)')
    plt.xlabel('Time (s)')
    # plt.legend(handles=patches)

    return (fig1, fig2, fig3)

def basicSimulation(pitHeight, slope, initialState=(0, 0, 0), mass=570000):
    # Runs a simulation according to 'simulation.py'.

    truck = Truck(slope, state=initialState, mass=mass)
    deltaT = 0.02

    # Perform simulation.
    while slope.getHeight(truck.state[0]) < pitHeight:
        truck.step(deltaT, (truck.powerFlow.maxGenPower, 0))

    return truck

def testOptimiserSimulator(optimiser, guess):
    # Test to make sure the optimiser's simulation is working.

    start = time.time()
    simTruck = optimiser.simulate(guess)
    end = time.time()
    print(end - start)

    start = time.time()
    basicTruck = basicSimulation(optimiser.pitHeight, optimiser.slope)
    end = time.time()
    print(end - start)

    # for i in range(len(simTruck.history)):
    #     print(simTruck.history[i])
    plotTruck(simTruck, 'r')
    plotTruck(basicTruck, 'b')
    plt.show()

def runOptimisation(optimiser, constraints, guess, bounds, obj=1,
                    targetAscentTime=0, eps=1e5):
    # Runs the optimiser.

    # Time the optimiser.
    start = time.time()  # Record the time.

    # Runs the optimiser
    res = optimiser.optimise(constraints, guess, bounds=bounds, obj=obj,
                             eps=eps, targetAscentTime=targetAscentTime)
    end = time.time()

    optimisedTruck = optimiser.simulate(res.x)  # Make a truck using the result.

    return (optimisedTruck, res, end-start, obj, optimiser)

def printOptimisationResults(result):
    # Prints the information relevant to an optimisation.

    (optimisedTruck, res, runTime, obj, optimiser) = result

    print("Optimised for objective: {}".format(obj))
    print(res)  # Print the result.
    print("Runtime: {}".format(runTime))  # Print time.
    print("Ascent time: {}s".format(optimisedTruck.time))
    print("Ascent height: {}m".format(optimisedTruck.slope.getHeight(
        optimisedTruck.history[-1][1])))
    print("Generator Energy Supplied: {}MJ".format(
        optimisedTruck.history[-1][6]/1e6))
    print("Cost: ${}".format(optimiser.calcEnergyToCost(
        optimisedTruck.history[-1][6])))
    print("Mass: {}kg".format(optimisedTruck.mass))

def paretoOptimiser(resultQueue, productMass, gradient, pitHeight, friction):
    # Method to run optimisations for different product masses,
    # slope gradients and friction coefficients.

    # Calculate the mass.
    totalMass = vehicleMass + productMass
    # Make a basic slope.
    angle = math.atan(gradient)
    brim = pitHeight/math.sin(angle)
    # gradients = ([0,brim-0.1,brim+200], [gradient,0,gradient]) # Includes a 200m flat section on the top of the pit.
    gradients = ([0,brim], [gradient,gradient])
    slope = Slope(gradients)

    # Make an optimiser.
    optimiser = Optimiser(slope, simStepTime=simStep, pitHeight=pitHeight,
                          optStepTime=optStep, state=initialState,
                          mass=totalMass)
    # Setup the optimisation.
    (guess, basicTruck, success) = optimiser.genGuess() # Must do before
    # generating constraints.
    if success:
        constraints = optimiser.genConstraints()
        bounds = optimiser.genBounds()
        # Run an optimisation for time (objective 3).
        res = runOptimisation(optimiser, constraints, guess, bounds, 3, eps=1e6)

        # Check that we made it up the slope.
        (optimisedTruck, result, time, objective, optimiser) = res
        height = optimisedTruck.slope.getHeight(optimisedTruck.history[-1][1])
        if height >= optimiser.pitHeight:
            # Add the result to the result queue.
            resultQueue.put(res)
            print('done')
            return True
        else:
            return False

    else:
        return False

def genReference(result):
    # Takes the parameters of an optimisation result and generates a
    # reference (ie. a simulation at max gen without the ERS).

    (time, productMass, totalMass, slope, pitHeight, friction) = result

    # Make an optimiser that will behave as just a simulator.
    optimiser = Optimiser(slope, simStepTime=simStep, pitHeight=pitHeight,
                          optStepTime=optStep, state=initialState,
                          mass=totalMass)
    # Use the genGuess function as a simulator. Will run the truck at max gen
    #  power and no ERS.
    return optimiser.genReference()

def plotPareto(resultList, curveColourDict):
    # Plots a Pareto curve of results.

    # Initialise a colour map.
    cmap = get_cmap(len(curveColourDict)+1) # '+1' so we don't get red twice.

    # Plot the data.
    fig = plt.figure(1)
    ax = plt.subplot(111)
    while len(resultList) > 0:
        result = resultList.pop()
        (time, productMass, totalMass, slope, pitHeight, friction) = result
        try:
            colour = cmap(curveColourDict[productMass])
        except:
            colour = 'k'
            curveColourDict[productMass] = colour
        (refTruck, success) = genReference(result) # Get reference truck.
        refTime = refTruck.time
        gradient = slope.getGradient(0)
        plt.plot(gradient, time, "o", color=colour)
        if success:
            plt.plot(gradient, refTime, "x", color=colour) # Plot the reference truck.
    # Sort the keys first so that they are in ascending order.
    keys = []
    for key in curveColourDict:
        keys.append(key)
    keys.sort()
    # Make the patches for the legend.
    paretoPatches = []
    for key in keys:
        try:
            colour = cmap(curveColourDict[key])
        except:
            colour = 'k'
        paretoPatches.append(mpatches.Patch(color=colour, label='{}'.format(key)))
    # Arrange the plot for the legend.
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width*0.8, box.height])
    plt.title('Sensitivity Curves for Gradient against Time at '
              'Varying Masses')
    plt.legend(handles=paretoPatches, loc='center left', bbox_to_anchor=(1,0.5))
    plt.ylabel('Time (s)')
    plt.xlabel('Gradient (%)')
    plt.grid()
    plt.show()

def worker(workQueue, resultQueue, maxTasks):
    # Performs the work on evaluating the optimisations.

    tasksCompleted = 0
    while tasksCompleted < maxTasks and workQueue.qsize() > 0:
        arg = workQueue.get(1)
        paretoOptimiser(*arg) # The star unpacks all the arguments 'args'.
        tasksCompleted += 1

    resultQueue.put('DONE')

def savePlot(optimiser, optimisedTruck, extractedResults, path):
    # Creates and saves a plot of an optimised truck.

    # optimisedTruck = optimiser.truck
    # Clear the plot and patches.
    plt.close('all')
    patches[:] = []
    # Generate the reference truck
    (refTruck, success) = genReference(extractedResults)
    if success:
        plotTruck(refTruck, 'k', 'Ref')
    # Plot the optimised truck.
    figs = plotTruck(optimisedTruck, 'r', 'Opt')
    # Generate the name of the plots.
    name = "mass{}_pit{}_grad{}_mu{}".format(optimisedTruck.mass-vehicleMass,
                                             optimiser.pitHeight,
                                             optimiser.slope.getGradient(0),
                                             optimiser.muRolling)
    name = name.strip('.')
    # Save the figures.
    for fig in figs:
        filePath = path + '/' + name + 'fig{}'.format(fig.number) + '.png'
        plt.figure(fig.number)
        plt.savefig(filePath)
        fig.clear()

def workerManager(dataWriter, resultQueue, workQueue, resultList,
                  extractedResultsList, processes, workers, maxTasks):
    # Manages the workers to ensure that results are saved and workers are
    # refreshed.

    cpuCount = multiprocessing.cpu_count()
    workersFinished = 0
    optimisationsFinished = 1
    complete = False
    while not workQueue.empty() or not complete:
        for result in iter(resultQueue.get, 'DONE'):
            # Process result.
            (optimisedTruck, res, runTime, obj, optimiser) = result
            extractedResults = (optimisedTruck.time,
                                optimisedTruck.mass-vehicleMass,
                                optimisedTruck.mass,
                                optimisedTruck.slope,
                                optimiser.pitHeight,
                                optimiser.muRolling)
            # Check whether the optimisation failed due to inequality
            # constraints. If so then we don't want to save it.
            if res.status != 4:
                dataWriter.saveResult(extractedResults)
            # Even if the optimisation failed we still want to save the plot.
            try:
                savePlot(optimiser, optimisedTruck, extractedResults,
                         dataWriter.path)
            except Exception as e:
                print('Plot save failed.')
                print(str(e))
            if not saveRamEnabled:
                resultList.append(result)
                extractedResultsList.append(extractedResults)
            print(optimisationsFinished)
            print("Result Status: {} - {}".format(res.status, res.message))
            gc.collect()
            print('Time: {}'.format(str(datetime.datetime.now().time())))
            print 'Memory usage: %s (kb)' % resource.getrusage(
                resource.RUSAGE_SELF).ru_maxrss
            optimisationsFinished += 1
        # Indicate that a worker completed.
        workersFinished += 1
        # Check whether the work is complete.
        if workQueue.empty() and workers-workersFinished==0:
            complete = True
        # Get and print information about the status of processes
        aliveProcesses = 0
        deadProcesses = 0
        for p in processes:
            p.join(0.005)
            if p.is_alive():
                aliveProcesses += 1
            else:
                deadProcesses += 1
        # Make new processes to replenish dead ones
        while cpuCount - aliveProcesses > 0 and not workQueue.empty():
            p = Process(target=worker,
                        args=(workQueue, resultQueue, maxTasks))
            p.start()
            processes.append(p)
            aliveProcesses += 1
            workers += 1
        print('Alive processes: {}').format(aliveProcesses)
        print('Dead processes: {}').format(deadProcesses)
    return processes

def main():
    # Runs optimisations for several problems and displays the results.

    print(gc.isenabled())

    productMass = 284000
    startGradient = 0.05
    gradient = startGradient
    friction = 0.02
    startPitHeight = 20
    pitHeight = startPitHeight
    i = 1
    curves = 0
    curveColourDict = {} # Maps the curve (such as pitHeight=70) to a colour.

    # Description of the optimisation.
    description = "Product rate against gradient with mass contours. \n " \
                  "Start Product: {}\n" \
                  "Start Gradient: {}\n" \
                  "Start Pit Height: {}m\n" \
                  "Start Friction: {}".format(productMass, startGradient,
                                              startPitHeight, friction)

    processes = []
    dataWriter = DataWriter(description)

    # Build a work queue for the workers to work through.
    result = Manager()
    work = Manager()
    resultQueue = result.Queue()
    workQueue = work.Queue()
    while productMass <= 384000:
        while gradient <= 0.12:
            workQueue.put((resultQueue, productMass, gradient, pitHeight,
                          friction))
            # Increment a performance parameter.
            gradient += 0.01
            print(i)
            i += 1
        curveColourDict[productMass] = curves
        productMass += 20000
        gradient = startGradient
        curves += 1

    # Save the colour dictionary.
    dataWriter.saveColourDict(curveColourDict)

    # Start the workers.
    workers = multiprocessing.cpu_count()
    maxTasks = 2 # Don't use 1.
    for i in xrange(workers):
        p = Process(target=worker, args=(workQueue,resultQueue,maxTasks))
        p.start()
        processes.append(p)

    # Start process handling data.
    resultList = []
    extractedResultsList = []
    processes = workerManager(dataWriter, resultQueue, workQueue, resultList,
                  extractedResultsList, processes, workers, maxTasks)

    # Terminate the workers and saver.
    for p in processes:
        p.join()

    # Plot the data if we saved the results in ram.
    if plottingEnabled and not saveRamEnabled:
        print(len(resultList))
        plotPareto(resultList, curveColourDict)

    print('Finished!!')

# def main():
#     # Perform only a few optimisations.
#
#     mass = 384000+vehicleMass
#     pitHeight = 200
#     initialState = (0,7,0)
#
#     # Run optimisation for 0.135% gradient.
#     gradient = 0.06
#     gradients = ([0],[gradient])
#     slope = Slope(gradients)
#     optimiser = Optimiser(slope, simStepTime=0.1, pitHeight=pitHeight,
#                           optStepTime=5, state=initialState, mass=mass)
#     (guess, basicTruck, success) = optimiser.genGuess()
#     if success:
#         constraints = optimiser.genConstraints()
#         bounds = optimiser.genBounds()
#         res = runOptimisation(optimiser, constraints, guess, bounds, 3, eps=1e6)
#         (optimisedTruck, result, time, objective, optimiser) = res
#         plotTruck(optimisedTruck, 'r', 'Grad 6')
#         printOptimisationResults(res)
#
#     # Run optimisation for 0.134% gradient.
#     gradient = 0.11
#     gradients = ([0],[gradient])
#     slope = Slope(gradients)
#     optimiser = Optimiser(slope, simStepTime=0.1, pitHeight=pitHeight,
#                           optStepTime=5, state=initialState, mass=mass)
#     (guess, basicTruck, success) = optimiser.genGuess()
#     if success:
#         constraints = optimiser.genConstraints()
#         bounds = optimiser.genBounds()
#         res = runOptimisation(optimiser, constraints, guess, bounds, 3, eps=1e6)
#         (optimisedTruck, result, time, objective, optimiser) = res
#         plotTruck(optimisedTruck, 'b', 'Grad 11')
#         printOptimisationResults(res)
#
#     plotTruck(basicTruck,'k', 'Basic')
#
#     plt.show()


if __name__ == '__main__':
    main()
