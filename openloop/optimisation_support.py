from simulation_support import *
import scipy.optimize as opt
import math
import numpy as np


class Optimiser():
    # Class used for optimising a dump truck's power inputs.

    # General variables that only need to be changed in this class.
    fuelEff = 205.7 / 3600000  # g/J ('/3600000' for conversion from g/kWh).
    fuelCost = 1.10  # $/L from aip.com.au terminal gate prices 2/12/16.
    fuelDensity = 840  # g/L

    def __init__(self, slope, simStepTime=0.02, optStepTime=0.5,
                 state=(0, 0, 0), pitHeight=100, mass=570000, muRolling=0.02,
                 guessPower = 2390000):
        # Creates an instance of an optimiser with the following parameters.

        self.pitHeight = pitHeight
        self.slope = slope
        self.initialEnergy = self.calcInitialEnergy(pitHeight)
        self.simTimeStep = simStepTime
        self.optTimeStep = optStepTime
        state = (state[0], state[1], self.initialEnergy)
        self.initialState = state
        self.mass = mass
        self.muRolling = muRolling
        self.truck = Truck(slope, state=state, mass=mass)
        self.simRes = ()
        self.powerFlow = PowerFlow()
        self.simulations = 0
        self.xSize = 0
        self.guessPower = guessPower

    def setSlope(self, slope):
        self.slope = slope

    def setPitHeight(self, pitHeight):
        self.pitHeight = pitHeight
        self.initialEnergy = self.calcInitialEnergy(pitHeight)

    def setInTimeStep(self, inputStepTime):
        self.optTimeStep = inputStepTime

    def calcInitialEnergy(self, pitHeight):
        # Calculates the initial energy of the truck based on the pit height.

        eWh = 463 * pitHeight  # Energy in Watt-hr
        return eWh * 3600  # Energy in Joules

    def calcEnergyToFuel(self, energy):
        # Calculates the amount of fuel (L) of an amount of energy (J).

        return energy * self.fuelEff / self.fuelDensity

    def calcFuelToCost(self, fuel):
        # Calculates the cost ($) of an amount of fuel (L).

        return fuel * self.fuelCost

    def calcEnergyToCost(self, energy):
        # Calculates the cost ($) of an amount of energy (J) assuming it is
        # entirely transformed to fuel.

        return self.calcFuelToCost(self.calcEnergyToFuel(energy))

    def simulate(self, x):
        # Performs a simulation using the parameters in the following code. The
        # input x is the powers supplied by the generator and ERS as a vector
        # with every even index a generator power and every odd index the ERS
        # power complimentary to the previous generator power.

        truck = Truck(self.slope, state=self.initialState, mass=self.mass,
                      muRolling=self.muRolling)

        # Perform the simulation. Importantly, the inputs should only change
        # according to the optTimeStep.
        i = 0
        sim2optSteps = int(math.floor(self.optTimeStep / self.simTimeStep))
        while i < len(x):
            j = 0
            while self.slope.getHeight(truck.state[0]) < self.pitHeight and \
                            j < sim2optSteps:
                truck.step(self.simTimeStep, (self.powerFlow.maxGenPower, x[i]))
                j += 1
            i += 1

        # if truck.state[2] < 0:
        #     print("Used too much ERS: {}".format(truck.state[2]))

        return truck

    def getSimResults(self, x):
        # Runs a simulation and formats the results for use in an objective
        # function.

        # Run the simulation.
        simTruck = self.simulate(x)

        # Organise the results of the simulation.
        t, h, v, genPower, ERSpower = [], [], [], [], []
        totGenEnergy = 0
        lastTime = 0
        unusedERS = self.initialEnergy
        for el in simTruck.history:
            t.append(el[0])
            h.append(self.slope.getHeight(el[1]))
            v.append(el[2])
            genPower.append(el[4])
            ERSpower.append(el[5])
            totGenEnergy += el[4] * (el[0] - lastTime)
            unusedERS -= el[5] * (el[0] - lastTime)
            lastTime = el[0]

        return (t, h, v, genPower, ERSpower, totGenEnergy, unusedERS)

    def objective1(self, x):
        # Objective is to minimise time and generator energy and maximise ERS
        # use.

        # Run a simulation and get the results.
        simRes = self.getSimResults(x)

        # Check that the truck made it up the slope.
        penalty = 0
        if simRes[1][-1] < self.pitHeight:
            penalty = 1e18

        # Minimise the energy consumed by the generator and the time taken to
        # consume that energy. (This is the objective function.)
        time = simRes[0][-1]
        totGenEnergy = simRes[5]
        ERSenergyLeft = simRes[6]
        obj = 10 * time * ERSenergyLeft + time * totGenEnergy + penalty

        print("obj={},time={},ERSleft={},GenUsed={}".format(obj, time,
                                                ERSenergyLeft,totGenEnergy))

        return obj

    def objective2(self, x):
        # Objective function to minimise the time by encouraging the use of
        # the ERS energy.

        # Run a simulation and get the results.
        simRes = self.getSimResults(x)

        # Check that the truck made it up the slope.
        penalty = 0
        if simRes[1][-1] < self.pitHeight:
            penalty = 1e18

        # Maximise the energy supplied by the ERS and the time taken to
        # consume that energy. (This is the objective function.)
        time = simRes[0][-1]
        ERSenergyLeft = simRes[6]
        obj = time * 1e9 + time * ERSenergyLeft + penalty

        print("obj={},time={},ERSleft={}".format(obj, time, ERSenergyLeft))

        return obj

    def objective3(self, x):
        # Objective function to minimise the time.

        # Run a simulation and get the results.
        simRes = self.getSimResults(x)

        # Check that the truck made it up the slope.
        penalty = 0
        if simRes[1][-1] < self.pitHeight:
            penalty = 1e18

        # Maximise the energy supplied by the ERS and the time taken to
        # consume that energy. (This is the objective function.)
        time = simRes[0][-1]
        ERSenergyLeft = simRes[6]
        obj = time * 1e9 + ERSenergyLeft * 1e2 + penalty

        # print("obj={},time={},ERSleft={}".format(obj, time, ERSenergyLeft))

        return obj

    def objective4(self, x):
        # Objective function to minimise the consumption of fuel. To
        # encourage this the consumption of energy from the ERS is maximised.

        # Run a simulation and get the results.
        simRes = self.getSimResults(x)

        # Check that the truck made it up the slope.
        penalty = 0
        if simRes[1][-1] < self.pitHeight:
            penalty = 1e18

        # Minimise the energy consumed by the generator and the time taken to
        # consume that energy. (This is the objective function.)
        time = simRes[0][-1]
        totGenEnergy = simRes[5]
        ERSenergyLeft = simRes[6]
        fuel = self.calcEnergyToFuel(ERSenergyLeft + totGenEnergy)
        obj = fuel * 1e9 + penalty

        print("obj={},time={},ERSleft={},GenUsed={},fuel={}".format(obj, time,
                                        ERSenergyLeft,totGenEnergy,fuel))

        return obj

    def objective5(self, x, ascentTime):
        # Objective function to minimise the consumption of fuel when
        # targeting a specific time. The target time needs to be set when
        # beginning the optimisation.

        # Run a simulation and get the results.
        simRes = self.getSimResults(x)

        # Check that the truck made it up the slope.
        penalty = 0
        if simRes[1][-1] < self.pitHeight:
            penalty = 1e18

        # Minimise the fuel consumed by the generator and the difference
        # between the target ascent time and actual ascent time.
        time = simRes[0][-1]
        totGenEnergy = simRes[5]
        ERSenergyLeft = simRes[6]
        fuel = self.calcEnergyToFuel(ERSenergyLeft + totGenEnergy)
        obj = abs(time - ascentTime) * 1e9 + fuel * 1e9 + penalty

        print(
            "obj={},time={},targetTime={},ERSleft={},fuel={}".format(obj, time,
                                                ascentTime,ERSenergyLeft,fuel))

        return obj

    def optimise(self, constr, guess, targetAscentTime=0, bounds=None,
                 obj=1, eps=1e5, iters = 50):
        # Performs an optimisation for the current parameters.
        if obj == 5:
            return opt.minimize(self.objective5, guess, constraints=constr,
                                bounds=bounds, method='SLSQP',
                                options={'eps': eps, 'maxiter': iters},
                                args=(targetAscentTime,))
        elif obj == 4:
            return opt.minimize(self.objective4, guess, constraints=constr,
                                bounds=bounds, method='SLSQP',
                                options={'eps': eps, 'maxiter': iters})
        elif obj == 3:
            return opt.minimize(self.objective3, guess, constraints=constr,
                                bounds=bounds, method='SLSQP',
                                options={'eps': eps, 'maxiter': iters})
        elif obj == 2:
            return opt.minimize(self.objective2, guess, constraints=constr,
                                bounds=bounds, method='SLSQP',
                                options={'eps': eps, 'maxiter': iters})
        else:
            return opt.minimize(self.objective1, guess, constraints=constr,
                                bounds=bounds, method='SLSQP',
                                options={'eps': eps, 'maxiter': iters})

    def sumERSpower(self, x, i):
        # Takes an input to the simulator and sums the ERS energy up to and
        # including the ith element of x.

        sum = 0
        j = 1
        while j <= i:
            sum += x[j]
            j += 1
        return sum

    def genEnergyLimitERS(self):
        # Ensures that the energy outputted by the ERS does not exceed the store.

        funs = [(lambda x, i=i: self.initialEnergy -
                                self.optTimeStep * self.sumERSpower(x, i))
                for i in range(self.xSize)]

        return [{'type': 'ineq', 'fun': funs[i]} for i in range(len(funs))]

    def genOutPowerLimitConstraints(self):
        # Ensures that the output powers never exceed the limit.

        # The maximum output power of the truck.
        maxOutPower = self.powerFlow.maxOutPower
        # Add the constraints for the max power limit.
        funs = [(lambda x, i=i: maxOutPower - self.powerFlow.powerOut((
                                    self.powerFlow.maxGenPower, x[i])))
                for i in range(self.xSize)]
        return [{'type': 'ineq', 'fun': funs[i]} for i in range(len(funs))]

    def genConstraints(self):
        # Generates the constraints for the optimisation problem. The guess
        # must have been generated before the constraints as the contraints
        # rely on the size of the input vector (xSize).

        constraints = []
        # Total power limit.
        constraints = self.genOutPowerLimitConstraints()
        constraints = constraints + self.genEnergyLimitERS()
        return constraints

    def genBounds(self):
        # Generates the sequence of bounds for the powers.

        bounds = []
        for i in range(self.xSize):
            bounds.append((0, self.powerFlow.maxERSpower))

        return bounds

    ## Generates a guess that tries and uses all of the ERS energy at a rate
    # of 50% MAX.
    def genGuess(self):
        # Generates the initial guess for an optimisation problem.

        truck = Truck(self.slope, state=self.initialState, mass=self.mass,
                      muRolling=self.muRolling)
        initialERSenergy = self.initialEnergy/4
        powerFlow = truck.getPowerFlow()

        # Perform simulation. Importantly, for the sake of memory, this can't
        #  be allowed to be an infinite loop.
        success = True
        while self.slope.getHeight(truck.state[0]) < self.pitHeight:
            # Calculate ERS contribution.
            ERSpower = 0
            ERSused = truck.getERSenergyUsed()
            if ERSused + powerFlow.maxERSpower*self.simTimeStep < initialERSenergy:
                ERSpower = powerFlow.maxERSpower
            elif ERSused < initialERSenergy:
                ERSpower = (initialERSenergy - ERSused)/self.simTimeStep
            # Use a max generator power and max ERS power (until it runs out).
            truck.step(self.simTimeStep, (self.guessPower, ERSpower))

            # If the trucks position is ever decreasing rather than
            # increasing, terminate the loop.
            if len(truck.history) > 10 and \
                            truck.history[-1][1]-truck.history[-2][1] < 0:
                print('Truck can\'t ascend.')
                success = False
                break

        # Use the updated truck from the simulation to get an initial guess
        # for the optimisation problem. It is imperative that the number of
        # elements in this guess is larger than what is required for the
        # actual problem as the optimiser is unable to change the size of the
        # inputs.
        # These next two lines gets the steps at which the optimiser inputs
        # occur and then grabs each of these.
        step = int(math.floor(self.optTimeStep / self.simTimeStep))/2
        guess = [truck.inputs[i][1] for i in range(0, len(truck.inputs), step)]

        self.xSize = len(guess)

        return (guess, truck, success)

    def genReference(self):
        # Generates a reference case where there is no ERS energy.

        truck = Truck(self.slope, state=self.initialState, mass=self.mass,
                      muRolling=self.muRolling)

        # Perform simulation. Importantly, for the sake of memory, this can't
        #  be allowed to be an infinite loop.
        success = True
        while self.slope.getHeight(truck.state[0]) < self.pitHeight:
            # Use a max generator power and max ERS power (until it runs out).
            truck.step(self.simTimeStep, (self.guessPower, 0))

            # If the trucks position is ever decreasing rather than
            # increasing, terminate the loop.
            if len(truck.history) > 10 and \
                            truck.history[-1][1]-truck.history[-2][1] < 0:
                print('Truck can\'t ascend.')
                success = False
                break

        return (truck, success)