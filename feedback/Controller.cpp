//
// Created by lachlan on 25/02/17.
//

#include <ctime>
#include <iostream>
#include <fstream>
#include "Controller.hpp"

Parameters *Controller::params = 0;
OptResult *Controller::optResult = 0;

// Constructor. Initialises all members.
Controller::Controller(Parameters *params, State &initialState,
                       OptResult *optResult) : initialState(initialState) {
    // Set static member params and optResult.
    Controller::params = params;
    Controller::optResult = optResult;
    // Set the root of the tree.
    root = new Branch(initialState);
}

// Performs the MCTS algorithm to build the tree until time runs out.
void Controller::runMCTS() {
    // Record the initial time.
    clock_t begin = clock();
    // Initialise a variable to be used to track time.
    double elapsed = 0;
    // Counter to observe the number of iterations performed.
    long counter = 0;
    // Perform the MCTS algorithm until time runs out.
    while (CONTROL_STEP_TIME > elapsed) {
        // Select from the root.
        Node *fringe = root->selectFringe();
        // Simulate from the new expansion. This relies on the optimisation.
        fringe->simulate();
        // Backpropagate the result from the new expansion. There is no need
        // to pass in a Node* as there should be no children in fringe.
        fringe->backPropagate(nullptr);
        // Calculate the elapsed time.
        elapsed = (clock() - begin) / CLOCKS_PER_SEC;
        // Increment the counter.
        counter++;
    }
}

// Return a copy of params.
Parameters *Controller::getParameters() {
    return Controller::params;
}

// Simulates a truck from the current state until it has reached the brim
// using the input and disturbance for the first stage then the optimised
// trajectory plus randomly generated disturbances for the remaining stages.
double Controller::simulate(Stage *stage) {
    // Extract the data from stage for convenience.
    Branch *branch = stage->branch;
    // Create a truck to be used for the simulation.
    Truck truck(params->slope, *branch->getState(), params->getTotalMass(),
                params->muRolling);
    // Get the input power.
    double inputPower = stage->input->getInputPower();
    // Get the disturbance force.
    double distForce = stage->dist->getDistForce();
    // Simulate the truck through the initial powers.
    simulateOptStep(&truck, inputPower, distForce);
    // Continue the simulation through the optimisation inputs. Add one to
    // the depth as the above inputPower and distForce were used instead.
    for (int i = branch->getDepth() + 1; i < optResult->x.size(); i++) {
        // Check whether the brim has been reached.
        if (truck.getHeight() >= params->pitHeight) {
            // Return the truck's time as the result.
            return truck.getTime();
        }
        // Simulate the next optimisation step.
        simulateOptStep(&truck, optResult->x[i], 0);
    }
    // If the optimisation inputs run out use the maximum ers power until the
    // brim is reached or the truck begins moving backwards (the latter
    // shouldn't happen). Note that 'simulateOptStep' ensures that it is not
    // possible to use more than the allowed ers energy.
    while (truck.getState().v > 0) {
        // Check whether the brim has been reached.
        if (truck.getHeight() >= params->pitHeight) {
            // Return the truck's time as the result.
            return truck.getTime();
        }
        // Simulate the next optimisation step.
        simulateOptStep(&truck, MAX_ERS_POWER, 0);
    }
    // If we get to here it means the truck was rolling back down the hill...
    // this is unacceptable so return the largest allowed value.
    return std::numeric_limits<double>().max();
}

// Performs a simulation of a single optimisation length step.
void Controller::simulateOptStep(Truck *truck, double inputPower,
                                 double distForce) {
    // Perform the simulation. Importantly, the inputs should only change
    // according to the optimisation time step (not the simulation time step).
    int simToOptSteps = (int) floor(params->optTimeStep / params->simTimeStep);
    // Iteratively step the truck through the inputs.
    for (int j = 0; j < simToOptSteps; j++) {
        // Break the simulation if the brim has been reached. It is not the
        // responsibility of this function to set the value and backpropagate
        // though...
        if (truck->getHeight() >= params->pitHeight) {
            // Simulation is complete so exit the function.
            return;
        }
        // Check that there is enough ers for next step.
        if (truck->getErsLeft() > inputPower * params->simTimeStep) {
            // Simulate the truck with the specified inputPower power.
            truck->simulateStep(params->simTimeStep,
                                InputPower(MAX_GEN_POWER, inputPower),
                                distForce);
        }
            // Handle the case where there isn't enough ers for a full power
            // inputPower.
        else if (truck->getErsLeft() > 0) {
            // Recalculate the inputPower to finish at zero.
            inputPower = truck->getErsLeft() / params->simTimeStep;
            // Simulate the truck with the recalculated inputPower power.
            truck->simulateStep(params->simTimeStep,
                                InputPower(MAX_GEN_POWER, inputPower),
                                distForce);
        }
            // Handle the case where there is no ers left.
        else {
            // Simulate the truck with the zero inputPower.
            truck->simulateStep(params->simTimeStep,
                                InputPower(MAX_GEN_POWER, 0), distForce);
        }
    }
}

// Perform a simulation of an ascent with disturbances and feedback control.
void Controller::run() {
    // Continuing stepping through the simulation until the root indicates
    // that it has ascended the pit.
    while (!root->isFinished()) {
        // Run the mcts algorithm to build the tree.
        runMCTS();
        // Pick a disturbance randomly to simulate measuring a disturbance.
        Disturbance *dist = root->getRandomDisturbance();
        // Get the best input response to this disturbance.
        Input *bestInput = root->getBestInput(dist);
        // Store the current root, disturbance and input as a stage.
        history.push_back(makeNextStage(root, dist, bestInput));
        // Print the latest stage.
        printStage((int) history.size() - 1);
        // Cleanup the root and begin building the tree again.
        root->cleanup(dist, bestInput);
        // Get the new root.
        root = bestInput->getChild();
        // Tell it that it is root.
        root->setAsRoot();
    }
}

void Controller::plot() {

}

// Save a control result.
void Controller::save(ControlResult *res, std::string filename) {
    // Delete any file that already exists with the same name.
    remove(filename.c_str());
    // Create a file to save the data into.
    ofstream outputFile;
    // Open the file.
    outputFile.open(filename);
    // Variable to calculate the time.
    double time = 0;
    // Get the history from the truck.
    std::vector<Step> history = res->truck->getHistory();
    // Iterate over the results.
    for (unsigned long i = 0; i < res->dists->size(); i++) {
        // Add a line to the file.
        outputFile << time << "," << res->dists->at(i) << ","
                   << res->inputs->at(i) << "," << history[i].state.s << "\n";
        // Increment the time.
        time += params->simTimeStep;
    }
    // Close the file.
    outputFile.close();
}

// Prints the results up to the current time.
void Controller::printHistory() {
    // Iterate over history.
    for (int i = 0; i < history.size(); i++) {
        printStage(i);
    }
}

// Prints a single stage in history if it exists.
void Controller::printStage(int index) {
    // Check if given index is within history.
    if (index < history.size()) {
        // Get the stage.
        Stage *stage = history[index];
        // Print the information about this stage nicely.
        std::cout << "Stage: " << index + 1 << "\n";
        std::cout << "Input: " << stage->input->getInputPower() << "\n";
        std::cout << "Disturbance: " <<
                  stage->dist->getDistForce() << "\n";
        State *state = stage->branch->getState();
        std::cout << "Initial State - s: " << state->s << ", v: " << state->v
                  << ", j: " << state->j << "\n";
        State *fState = stage->input->getChild()->getState();
        std::cout << "Final State   - s: " << fState->s << ", v: " << fState->v
                  << ", j: " << fState->j << "\n";
        double currentTime = (index + 1) * params->optTimeStep;
        // Check whether this stage is finished and if so print the finishing
        // time.
        if (stage->input->getChild()->isFinished()) {
            std::cout << "Finished!! Ascent time: "
                      << stage->input->getChild()->getTime() << "\n" << "\n";
        }
            // Otherwise print the status times.
        else {
            std::cout << "Ascent time elapsed: " << currentTime << "\n";
            std::cout << "Expected ascent time: "
                      << stage->input->getValue() << "\n" << "\n";
        }
    } else {
        std::cout << "No stage of control history at index: " << index << "\n";
    }
}

// Makes a copy of all the elements of a stage and stores them in a new stage.
Stage *Controller::makeNextStage(Branch *branch, Disturbance *dist,
                                 Input *input) {
    // Make copies of all the elements.
    Disturbance *newDist = new Disturbance(dist, branch, input);
    Input *newInput = new Input(input, dist);
    // Check whether there is history.
    Branch *newBranch;
    if (history.size() > 0) {
        // Make a new branch using the history.
        newBranch = new Branch(branch, history.back()->input, dist);
    } else {
        // Make a new branch with a null pointer for the parent.
        newBranch = new Branch(branch, nullptr, dist);
    }
    // Make the next stage.
    return new Stage(newInput, newDist, newBranch);
}

// Runs a simulation to generate a trajectory under open-loop control.
ControlResult *Controller::simulateFeedback() {
    // Make a truck.
    Truck *truck = new Truck(params->slope,
                             *history.front()->branch->getState(),
                             params->getTotalMass(), params->muRolling);
    // Perform the simulation. Importantly, the inputs should only change
    // according to the optimisation time step (not the simulation time step).
    int simToOptSteps = (int) floor(params->optTimeStep / params->simTimeStep);
    std::vector<double> *simInputs = new std::vector<double>();
    std::vector<double> *simDists = new std::vector<double>();
    for (const auto stage : history) {
        // Get the input power and disturbance force.
        double inputPower = stage->input->getInputPower();
        double distForce = stage->dist->getDistForce();
        for (int j = 0; j < simToOptSteps; j++) {
            // Check whether the height has been reached.
            if (truck->getHeight() >= params->pitHeight) {
                // Return the results.
                return new ControlResult(truck->getTime(), truck->getHeight(),
                                         truck->getGenEnergyUsed(),
                                         truck->getErsLeft(), simInputs,
                                         simDists, truck);
            }
            // Check that the input won't exceed the ers limitations.
            // Check that there is enough ers for next step.
            if (truck->getErsLeft() > inputPower * params->simTimeStep) {
                // Do nothing as the inputPower can be left as is.
            }
                // Handle the case where there isn't enough ers for a full power
                // inputPower.
            else if (truck->getErsLeft() > 0) {
                // Recalculate the inputPower to finish at zero.
                inputPower = truck->getErsLeft() / params->simTimeStep;
            }
                // Handle the case where there is no ers left.
            else {
                // Set the input power to zero.
                inputPower = 0;
            }
            // Record the input and force.
            simInputs->push_back(inputPower);
            simDists->push_back(distForce);
            // Run a simulation step.
            truck->simulateStep(params->simTimeStep,
                                InputPower(MAX_GEN_POWER, simInputs->back()),
                                simDists->back());
        }

    }
    // Return the results.
    return new ControlResult(truck->getTime(), truck->getHeight(),
                             truck->getGenEnergyUsed(), truck->getErsLeft(),
                             simInputs, simDists, truck);
}

// Runs a simulation to generate a trajectory under open-loop control.
ControlResult *Controller::simulateOpenloop() {
    // Make a truck.
    Truck *truck = new Truck(params->slope,
                             *history.front()->branch->getState(),
                             params->getTotalMass(), params->muRolling);
    // Perform the simulation. Importantly, the inputs should only change
    // according to the optimisation time step (not the simulation time step).
    int simToOptSteps = (int) floor(params->optTimeStep / params->simTimeStep);
    std::vector<double> *simInputs = new std::vector<double>();
    std::vector<double> *simDists = new std::vector<double>();
    for (int i = 0; i < history.size(); i++) {
        // Get the disturbance force.
        double distForce = history[i]->dist->getDistForce();
        // Get the input power.
        double inputPower;
        // Check whether the i is still within the bounds of the optimal
        // trajectory.
        if (i < optResult->x.size()) {
            // Use the power from the optimal trajectory.
            inputPower = optResult->x[i];
        } else {
            // Use no input power.
            inputPower = 0;
        }
        // Simulate this input.
        for (int j = 0; j < simToOptSteps; j++) {
            // Check whether the height has been reached.
            if (truck->getHeight() >= params->pitHeight) {
                // Return the results.
                return new ControlResult(truck->getTime(), truck->getHeight(),
                                         truck->getGenEnergyUsed(),
                                         truck->getErsLeft(), simInputs,
                                         simDists, truck);
            }
            // Check that the input won't exceed the ers limitations.
            // Check that there is enough ers for next step.
            if (truck->getErsLeft() > inputPower * params->simTimeStep) {
                // Do nothing as the inputPower can be left as is.
            }
                // Handle the case where there isn't enough ers for a full power
                // inputPower.
            else if (truck->getErsLeft() > 0) {
                // Recalculate the inputPower to finish at zero.
                inputPower = truck->getErsLeft() / params->simTimeStep;
            }
                // Handle the case where there is no ers left.
            else {
                // Set the input power to zero.
                inputPower = 0;
            }
            // Record the input and force.
            simInputs->push_back(inputPower);
            simDists->push_back(distForce);
            // Run a simulation step.
            truck->simulateStep(params->simTimeStep,
                                InputPower(MAX_GEN_POWER, simInputs->back()),
                                simDists->back());
        }

    }
    // Return the results.
    return new ControlResult(truck->getTime(), truck->getHeight(),
                             truck->getGenEnergyUsed(), truck->getErsLeft(),
                             simInputs, simDists, truck);
}

// Prints results of a control trajectory result.
void Controller::printResults(ControlResult *controlResult) {
    std::cout << "\n";
    std::cout << "Ascent time: " << controlResult->time << "\n";
    std::cout << "Height: " << controlResult->height << "\n";
    std::cout << "ERS energy left: " << controlResult->ersLeft << "\n";
    // Get the sum of the disturbance forces to offer an indication of
    // whether it was biased one way of another.
    double sum = 0;
    for (unsigned long i = 0; i < controlResult->dists->size(); i++) {
        sum += controlResult->dists->at(i);
    }
    std::cout << "Disturbance force sum: " << sum << "\n";
}

// Prints information about the edges (ie. the changes between one stage and
// the next).
void Controller::printEdgeInformation() {
    // Count the number of like edges between the inputs and disturbances.
    int likeEdges = 0;
    int numEdges = 0;
    for (unsigned long i = 1; i < history.size(); i++) {
        // Only count cases where there is ers energy left.
        if (history[i]->branch->getState()->j > ERS_LEFT_TOLERANCE) {
            // Determine whether the edges are rising or falling.
            bool inputRising = history[i]->input->getInputPower() -
                               history[i - 1]->input->getInputPower() > 0;
            bool distRising = history[i]->dist->getDistForce() -
                              history[i - 1]->dist->getDistForce() > 0;
            // If both are rising or both falling then it is a like edge.
            if ((inputRising && distRising) || (~inputRising && ~distRising))
                likeEdges++;
        }
        numEdges++;
    }
    // Print the information.
    std::cout << "\n" << "\n" << "Edge Information" << "\n";
    std::cout << "Total edges: " << numEdges << "\n";
    std::cout << "Like edges: " << likeEdges << "\n";
    std::cout << "Percent like edges: " << (double) 100 * likeEdges / numEdges
              << "%" << "\n";
}

// Performs a feedback control algorithm with an optimisation run on every
// stage.
ControlResult *Controller::runOptimisationFeedback() {
    // Stores the inputs that are selected by the optimiser.
    std::vector<double> *inputs = new std::vector<double>();
    // Stores disturbance forces (redundant but convenient).
    std::vector<double> *dists = new std::vector<double>();
    // Stores the starting state of the optimiser for every optimisation.
    State state = *history.front()->branch->getState();
    // Counter to allow the appropriate disturbance to be accessed from history.
    unsigned long counter = 0;
    // Continue iterating until complete or rolling wrong way.
    while (state.v > 0) {
        // Create an optimiser.
        Optimiser optimiser(*params, state, true);
        // Perform the optimisation.
        OptResult optResult =
                optimiser.optimise(obj1, std::numeric_limits<int>::max(),
                                                 CONTROL_STEP_TIME);
        // Calculate the next state.
        // Create a truck.
        Truck truck(params->slope, state, params->getTotalMass(),
                    params->muRolling);
        // Get the input power.
        double inputPower = optResult.x.front();
        // Get the disturbance corresponding to this stage.
        double distForce = 0;
        // If there are still disturbance forces use them (otherwise use 0).
        if (counter < history.size()) {
            distForce = history[counter]->dist->getDistForce();
        }
        // Simulate for one optimisation step.
        simulateOptStep(&truck, inputPower, distForce);
        // Set the new state using the final state of the truck.
        state = truck.getState();
        // Store the data.
        inputs->push_back(inputPower);
        dists->push_back(distForce);
        // Check whether the brim was reached.
        if (truck.getHeight() >= params->pitHeight) {
            // Simulate and return the results.
            return simulateOptFeedback(inputs, dists);
        }
        // Print some status information.
        std::cout << "Stage: " << counter << "\n";
        std::cout << "Input: " << inputPower << "\n";
        std::cout << "Disturbance: " << distForce << "\n";
        std::cout << "ERS left: " << state.j << "\n";
        std::cout << "Ascent time elapse: " << counter * params->optTimeStep
                  << "\n" << "\n";
        // Increment counter.
        counter++;
    }
    throw std::runtime_error("Didn't complete the optimisation based control.");
}

// Simulates the results from an optimisation feedback and return the
// collated results.
ControlResult *Controller::simulateOptFeedback(vector<double> *inputs,
                                               vector<double> *dists) {
    // Make a truck.
    Truck *truck = new Truck(params->slope,
                             *history.front()->branch->getState(),
                             params->getTotalMass(), params->muRolling);
    // Perform the simulation. Importantly, the inputs should only change
    // according to the optimisation time step (not the simulation time step).
    int simToOptSteps = (int) floor(params->optTimeStep / params->simTimeStep);
    std::vector<double> *simInputs = new std::vector<double>();
    std::vector<double> *simDists = new std::vector<double>();
    for (unsigned long i = 0; i < inputs->size(); i++) {
        // Get the disturbance force.
        double distForce = dists->at(i);
        // Get the input power.
        double inputPower = inputs->at(i);
        // Simulate this input.
        for (int j = 0; j < simToOptSteps; j++) {
            // Check whether the height has been reached.
            if (truck->getHeight() >= params->pitHeight) {
                // Return the results.
                return new ControlResult(truck->getTime(), truck->getHeight(),
                                         truck->getGenEnergyUsed(),
                                         truck->getErsLeft(), simInputs,
                                         simDists, truck);
            }
            // Check that the input won't exceed the ers limitations.
            // Check that there is enough ers for next step.
            if (truck->getErsLeft() > inputPower * params->simTimeStep) {
                // Do nothing as the inputPower can be left as is.
            }
                // Handle the case where there isn't enough ers for a full power
                // inputPower.
            else if (truck->getErsLeft() > 0) {
                // Recalculate the inputPower to finish at zero.
                inputPower = truck->getErsLeft() / params->simTimeStep;
            }
                // Handle the case where there is no ers left.
            else {
                // Set the input power to zero.
                inputPower = 0;
            }
            // Record the input and force.
            simInputs->push_back(inputPower);
            simDists->push_back(distForce);
            // Run a simulation step.
            truck->simulateStep(params->simTimeStep,
                                InputPower(MAX_GEN_POWER, simInputs->back()),
                                simDists->back());
        }

    }
    // Return the results.
    return new ControlResult(truck->getTime(), truck->getHeight(),
                             truck->getGenEnergyUsed(), truck->getErsLeft(),
                             simInputs, simDists, truck);
}
