#include <iostream>

#include "Optimiser.hpp"
#include "Slope.hpp"
#include "Truck.hpp"
#include "BoundedInteger.hpp"
#include "Defines.hpp"
#include "Disturbance.hpp"
#include <cmath>
#include <limits>

using namespace std;

//// For simulations.
//int main() {
//    // Make a slope.
//    map<double, double> grads;
//    grads[0.0] = 0.1;
//    Slope slope = Slope(grads);
//    // Make an initial state.
//    State initialState;
//    initialState.v = 8;
//    initialState.s = 0;
//    initialState.j = 0;
//    // Set rolling friction coefficient.
//    double muRolling = 0.02;
//    // Make a truck.
//    Truck truck = Truck(slope, initialState, 570000, muRolling);
//
//    // Parameters for the optimisation.
//    double pitHeight = 100;
//    double deltaT = 0.02;
//
//    // Perform the simulation until the truck ascends the slope.
//    clock_t begin = clock();
//    while (slope.getHeight(truck.getState().s) < pitHeight) {
//        // Setup the input.
//        InputPower inputPower(2370000, 0);
//        // Apply the input.
//        truck.simulateStep(deltaT, inputPower);
//    }
//    clock_t end = clock();
//
//    // Print the time taken to ascend.
//    cout << truck.getTime() << endl;
//    cout << double(end - begin) / CLOCKS_PER_SEC;
//
//    return 0;
//}

//// For optimisations.
//int main() {
//
//    // Make a slope.
//    map<double, double> grads;
//    grads[0.0] = 0.1;
//    Slope slope = Slope(grads);
//    // Make an initial state.
//    State initialState;
//    initialState.v = 8;
//    initialState.s = 0;
//    initialState.j = 0;
//    // Setup the optimisation parameters.
//    Parameters params(slope);
//    params.simTimeStep = 0.1;
//    params.optTimeStep = 5;
//    params.gradient = slope.getGradient(0);
//    params.muRolling = 0.02;
//    params.pitHeight = 100;
//    params.productMass = 384000;
//
//    // Initialise the optimiser.
//    Optimiser optimiser(params, initialState);
//
//    // Run the optimisation.
//    clock_t begin = clock();
//    OptResult optResult = optimiser.optimise(obj1, 50000);
//    clock_t end = clock();
//    // Simulate the optimisation to get results like the time.
//    SimResult simResult = optimiser.simulate(optResult.x);
//
//    // Print the time taken to ascend the slope.
//    cout << "Ascent time: " << simResult.time << "\n";
//    cout << "Optimisation status: " << optResult.optResult << "\n";
//    cout << "Runtime: " << double(end - begin) / CLOCKS_PER_SEC << "\n";
//    cout << "Initial energy: " << optimiser.getInitialEnergy() << "\n";
//    cout << "Sim ers left: " << optimiser.getInitialEnergy() -
//            simResult.ersEnergyUsed << "\n";
//    cout << "Calculated ers left: " <<
//         optimiser.calcErsLeft(optResult.x, optimiser.getSizeX()) << "\n";
//    cout << "Inputs:" << "\n";
//    for (int i = 0; i < optResult.x.size(); i++) {
//        cout << optResult.x[i] << "\n";
//    }
//
//    return 0;
//}

//// Used for testing.
//int main() {
//
////    inputId_t someInt(10);
////
////    inputId_t someOtherInt(101);
////
////    std::cout << someInt.val;
//
////    long double test = 1.999999999999999999999;
////    std::cout << truncl(test) << "\n";
////    std::cout << std::numeric_limits<float>::min() << "\n";
////    std::cout << 0.00000000000000000009;
////    std::cout << std::numeric_limits<double>().lowest();
//
//
////    // Make a branch.
////    Branch *b = new Branch(State());
////    // Make a few disturbances.
////    distId_t id1(10);
////    Disturbance *d1 = new Disturbance(b, id1);
////    distId_t id2(11);
////    Disturbance *d2 = new Disturbance(b, id2);
////    distId_t id3(12);
////    Disturbance *d3 = new Disturbance(b, id3);
//
////    // Test using maps.
////    std::map<distId_t, Disturbance *, CompareDistIds> dists;Disturbance(b, id3);
////    // Insert all the disturbances.
////    dists.insert(std::pair<distId_t, Disturbance *>(d1->getId(), d1));
////    std::cout << dists.size() << "\n";
////    dists.insert(std::pair<distId_t, Disturbance *>(d2->getId(), d2));
////    std::cout << dists.size() << "\n";
////    dists.insert(std::pair<distId_t, Disturbance *>(d3->getId(), d3));
////    std::cout << dists.size() << "\n";
////    // Insert a duplicate element.
////    Disturbance *d4 = dists.at(id1);
////    d4->setValue(1000);
////    Disturbance *d1a = new Disturbance(b, id1);
////    dists.insert(std::pair<distId_t, Disturbance *>(d1->getId(), d1a));
////    Disturbance *d5 = dists.at(id1);
////    std::cout << d5->getValue() << "\n";
//
////    // Test using a set.
////    std::set<Disturbance *, CompareNodeValue> distSet;
////    // Set the values of all the disturbance.
////    d1->setValue(100);
////    d2->setValue(1000);
////    d3->setValue(10000);
////    // Add the disturbances to the set.
////    distSet.insert(d1);
////    distSet.insert(d2);
////    distSet.insert(d3);
////    // Print the values of the elements from the set in the order they appear.
////    for (auto *i : distSet) {
////        std::cout << "id: " << i->getId().val << ", val: " << i->getValue() <<
////                                                                         "\n";
////    }
////    // Test what adding a duplicate does.
////    distSet.insert(d1);
////    // Print the values of the elements from the set in the order they appear.
////    for (auto *i : distSet) {
////        std::cout << "id: " << i->getId().val << ", val: " << i->getValue() <<
////                  "\n";
////    }
////    // Remove an disturbance.
////    distSet.erase(d1);
////    // Print the values of the elements from the set in the order they appear.
////    for (auto *i : distSet) {
////        std::cout << "id: " << i->getId().val << ", val: " << i->getValue() <<
////                  "\n";
////    }
////    // Check that the removed disturbance wasn't deleted.
////    std::cout << d1->getValue() << "\n";
//
////    // Test for randomly generating ids.
////    for (int i = 0; i < 100; i++) {
////        std::cout << Input::genRandInputId().val << "\n";
////    }
//
////    // Test what printing a struct looks like.
////    State state;
////    std::cout << state << "\n"; // compiler error
//
//    return 0;
//}

void printOptimisationResults(OptResult &optResult, Optimiser &optimiser,
                              double begin, double end) {
    // Simulate the optimisation to get results like the time.
    SimResult simResult = optimiser.simulate(optResult.x);

    // Print the time taken to ascend the slope.
    cout << "Ascent time: " << simResult.time << "\n";
    cout << "Optimisation status: " << optResult.optResult << "\n";
    cout << "Runtime: " << (end - begin) / CLOCKS_PER_SEC << "\n";
    cout << "Initial energy: " << optimiser.getInitialEnergy() << "\n";
    cout << "Sim ers left: " << optimiser.getInitialEnergy() -
            simResult.ersEnergyUsed << "\n";
    cout << "Calculated ers left: " <<
         optimiser.calcErsLeft(optResult.x, optimiser.getSizeX()) << "\n";
    cout << "Inputs:" << "\n";
    for (int i = 0; i < optResult.x.size(); i++) {
        cout << optResult.x[i] << "\n";
    }
    cout << "\n" << "\n";
 }

// For running feedback control.
int main() {
    // Setup the parameters of the slope.
    // Make a slope.
    map<double, double> grads;
    grads[0.0] = 0.1;
//    grads[500] = 0;
//    grads[700] = 0.1;
    Slope slope = Slope(grads);
    // Setup the optimisation parameters.
    Parameters params(slope);
    params.simTimeStep = 0.1;
    params.optTimeStep = 5;
    params.gradient = slope.getGradient(0);
    params.muRolling = 0.02;
    params.pitHeight = 100;
    params.productMass = 384000;
    // Make an initial state.
    State initialState;
    initialState.v = 8;
    initialState.s = 0;
    initialState.j = 0;

    // Perform an optimisation to get the optimal trajectory.
    // Initialise the optimiser.
    Optimiser optimiser(params, initialState);
    // Run the optimisation.
    clock_t begin = clock();
    OptResult optResult = optimiser.optimise(obj1, MAX_ITERS);
    clock_t end = clock();
    // Print the results.
    printOptimisationResults(optResult, optimiser, begin, end);

    // Run the feedback controller.
    // Set the initial energy correctly in initial state.
    initialState.j = optimiser.getInitialEnergy();
    // Initialise the controller.
    Controller controller(&params, initialState, &optResult);
    // Run controller.
    controller.run();
    // Run the optimisation based feedback strategy.
    ControlResult *optFeedbackResult = controller.runOptimisationFeedback();
    // Run simulation of the feedback trajectory.
    ControlResult *feedbackResult = controller.simulateFeedback();
    // Run simulation of the openloop trajectory.
    ControlResult *openloopResult = controller.simulateOpenloop();
    // Print the simulation results of the feedback and openloop trajectories.
    std::cout << "Feedback Results";
    controller.printResults(feedbackResult);
    std::cout << "\n" << "\n" << "Openloop Results";
    controller.printResults(openloopResult);
    std::cout << "\n" << "\n" << "Optimisation Feedback Results";
    controller.printResults(optFeedbackResult);
    // Print the edge information.
    controller.printEdgeInformation();
    // Save all sets of results.
    controller.save(feedbackResult, "feedback.csv");
    controller.save(openloopResult, "openloop.csv");
    controller.save(optFeedbackResult, "optfeedback.csv");
    return 0;
}