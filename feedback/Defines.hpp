//
// Created by lachlan on 26/02/17.
//

#ifndef FEEDBACK_DEFINES_HPP
#define FEEDBACK_DEFINES_HPP

#include "BoundedInteger.hpp"

#define DISTURBANCE_RESOLUTION      20
#define DISTURBANCE_MAX             100000
#define DISTURBANCE_MIN             -100000
#define INPUT_RESOLUTION            20
#define INPUT_MAX                   MAX_ERS_POWER
#define INPUT_MIN                   MIN_POWER

#define CONTROL_STEP_TIME           1

#define MAX_ITERS                   500

#define ERS_LEFT_TOLERANCE          1

#define EXPLORE                     0.9
#define EXPLOIT                     0.1

typedef BoundedInteger<0, DISTURBANCE_RESOLUTION> distId_t;
typedef BoundedInteger<0, INPUT_RESOLUTION> inputId_t;

#endif //FEEDBACK_DEFINES_HPP
