//
// Created by lachlan on 8/02/17.
//

#include "Truck.hpp"

#include <cmath>

using namespace std;

// Constructor. Assigns values to class members.
Truck::Truck(Slope slope, State initialState, double mass, double muRolling) :
        slope(slope), state(initialState), mass(mass), muRolling(muRolling) {
    // Make the first element of history.
    Step step(0, initialState, InputPower(0, 0), 0, 0);
    history.push_back(step);
}

// Makes a simulation step of the truck with no disturbance force.
void Truck::simulateStep(double timeStep, InputPower inputPower) {
    simulateStep(timeStep, inputPower, 0.0);
}

// Makes a simulation step of the truck with a disturbance force.
void Truck::simulateStep(double timeStep, InputPower inputPower,
                         double disturbanceForce) {

    // Ensure that the powers adhere to the limits.
    applyPowerLimits(inputPower);
    // Determine the power supplied to the wheels.
    double outputPower = calcAppliedPower(inputPower);

    // Calculate the rate of change of the state.
    // Velocity.
    double velocity = state.v;
    // Acceleration.
    double tractionF = calcTractionForce(outputPower);
    double angle = slope.getAngle(state.s);
    double gravityF = mass * g * sin(angle);
    double frictionF = mass * g * muRolling * cos(angle);
    double netF = calcNetForce(tractionF, gravityF, frictionF,
                               disturbanceForce);
    double acceleration = netF / mass;
    // ERS power used.
    double ersPowerUsed = -inputPower.ers;

    // Update state to reflect the changes.
    state.s = state.s + velocity * timeStep;
    state.v = state.v + acceleration * timeStep;
    state.j = state.j + ersPowerUsed * timeStep;

    // Update the history to reflect the changes.
    // Make a step.
    Step step(history.back().time + timeStep, state, inputPower,
              history.back().ersUsed + inputPower.ers * timeStep,
              history.back().genUsed + inputPower.gen * timeStep);
    history.push_back(step);
}

// Gets the generator energy used.
const double Truck::getGenEnergyUsed() {
    return history.back().genUsed;
}

// Gets the ers energy used.
const double Truck::getErsUsed() {
    return history.back().ersUsed;
}

// Returns a copy of the state of the truck.
State Truck::getState() {
    return state;
}

// Returns a constant reference to history.
const vector<Step> Truck::getHistory() {
    return history;
}

// Returns the ers energy left in the truck.
double Truck::getErsLeft() {
    return history.front().state.j - history.back().ersUsed;
}

// Returns the current time of the truck.
double Truck::getTime() {
    return history.back().time;
}

// Returns the current height of the truck.
const double Truck::getHeight() {
    return slope.getHeight(state.s);
}

// Calculates the net force that the truck applies to propel itself.
double Truck::calcNetForce(double tractionF, double gravityF, double frictionF,
                           double disturbanceF) {
    // Calculate the base net force without friction.
    double netF = tractionF - gravityF + disturbanceF;
    // Determine the direction friction acts in and apply it.
    // Stationary case.
    if (state.v == 0) {
        // Positive net force.
        if (netF >= 0) {
            // Friction applied in opposite direction.
            netF -= frictionF;
            // If net force is still positive return it if not there is no
            // force.
            if (netF >= 0) {
                return netF;
            } else {
                return 0;
            }
        }
            // Negative net force.
        else {
            // Friction applied in opposite direction.
            netF += frictionF;
            // If net force is still negative return it if not there is no
            // force.
            if (netF <= 0) {
                return netF;
            } else {
                return 0;
            }
        }
    }
        // Forward motion case.
    else if (state.v > 0) {
        return netF - frictionF;
    }
        // Reverse motion case.
    else {
        return netF + frictionF;
    }
}

// Calculates the traction (applied) force that is generated using the
// applied power and rim-pull curve.
double Truck::calcTractionForce(double appliedPower) {
    // Saturation linearisation parameters.
    double m = 73509.9; // Ns/m
    double c = 1311000.0; // N

    // Get the velocity at which the curve saturates.
    double satVelocity = calcSaturationVelocity(appliedPower, m, c);

    // Determine the force according to the rim-pull curve (a piecewise
    // function).
    // If the current velocity is not saturated.
    if (state.v > satVelocity) {
        // The standard relationship: F = P/v, applies.
        return appliedPower / state.v;
    }
        // If the current velocity is saturated.
    else {
        // Use the saturated linearisation of the rim-pull curve.
        return c - state.v * m;
    }
}

// Calculates the velocity below which the rim-pull curve becomes saturated.
double Truck::calcSaturationVelocity(double appliedPower, double m, double c) {
    return (c - sqrt(pow(c, 2) - 4 * m * appliedPower)) / (2 * m);
}

// Takes inputPower and ensures that its elements are within the allowable
// power ranges.
void Truck::applyPowerLimits(InputPower &inputPower) {
    // Check minimum.
    if (inputPower.ers < MIN_POWER) {
        inputPower.ers = MIN_POWER;
    }
    if (inputPower.gen < MIN_POWER) {
        inputPower.gen = MIN_POWER;
    }
    // Check maximum.
    if (inputPower.ers > MAX_ERS_POWER) {
        inputPower.ers = MAX_ERS_POWER;
    }
    if (inputPower.gen > MAX_GEN_POWER) {
        inputPower.gen = MAX_GEN_POWER;
    }
}

// Determines the power outputted to the wheels given inputPower.
double Truck::calcAppliedPower(const InputPower &inputPower) {
    // Calculate the output power using the efficiencies.
    double outputPower = OUT_DC_EFF *
                         (ERS_DC_EFF * inputPower.ers +
                          GEN_DC_EFF * inputPower.gen);

    // Reduce outputPower below the maximum and return it.
    return (outputPower < MAX_OUT_POWER) ? outputPower : MAX_OUT_POWER;
}
