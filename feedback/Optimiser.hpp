//
// Created by lachlan on 23/02/17.
//

#ifndef FEEDBACK_OPTIMISER_HPP
#define FEEDBACK_OPTIMISER_HPP

#include <nlopt.hpp>
#include "Slope.hpp"
#include "Truck.hpp"

using namespace std;

class Optimiser;

// The physical, temporal and precision parameters around which an optimisation
// is performed.
typedef struct Parameters {

    const double vehicleMass = 250000;
    const double guessPower = MAX_GEN_POWER;

    Slope slope;
    double gradient = 0.1;
    double pitHeight = 100;
    double productMass = 384000;
    double muRolling = 0.02;
    double targetAscentTime = 500;

    double simTimeStep = 0.1;
    double optTimeStep = 5;
    const double tolerance = 1e-2;
    const double epsilon = 1e7;

    const double getTotalMass() {
        return vehicleMass + productMass;
    }

    Parameters(Slope slope) : slope(slope) {}

} Parameters;

typedef struct SimResult {
    const double time;
    const double height;
    const double genEnergyUsed;
    const double ersEnergyUsed;

    SimResult(double time, double height, double genEnergyUsed,
              double ersEnergyUsed) :
            time(time), height(height), genEnergyUsed(genEnergyUsed),
            ersEnergyUsed(ersEnergyUsed) {}
} SimResult;

typedef struct OptResult {
    std::vector<double> &x;
    nlopt::result &optResult;

    OptResult(std::vector<double> &x, nlopt::result &optResult) :
            x(x), optResult(optResult) {}
} OptResult;

typedef enum ObjFunc {
    obj1
} ObjFunc;

typedef struct OptArgs {
    ObjFunc objFunc;
    Optimiser *optimiser;

    OptArgs(ObjFunc objFunc, Optimiser *optimiser) : objFunc(objFunc),
                                                     optimiser(optimiser) {}
} OptArgs;

typedef struct ConstraintArgs {
    Optimiser *optimiser;
    int index;

    ConstraintArgs(Optimiser *optimiser, int index) :
            optimiser(optimiser), index(index) {}
} ConstraintArgs;

class Optimiser {

    // g/J ('/3600000' for conversion from g/kWh.)
    const double fuelEff = 205.7 / 3600000;
    // $/L from aip.com.au terminal gate prices 2/12/16.
    const double fuelCost = 1.10; //
    // g/L
    const double fuelDensity = 840;

    const Parameters params;
    const State initialState;
    const double initialEnergy;
    const double mass;
    unsigned int xSize;

    public:
    Optimiser(Parameters params, State initialState);

    Optimiser(Parameters params, State initialState, bool);

    double calcEnergyToFuel(double energy);

    double calcEnergyToCost(double energy);

    OptResult optimise(ObjFunc objFunc, int maxIters);

    SimResult simulate(const vector<double> &x);

    double objFunc1(const vector<double> &x, vector<double> &jac);

    double ersLimitConstraint(const vector<double> &x, vector<double> &jac,
                              int index);

    unsigned int getSizeX(void);

    double getInitialEnergy(void);

    double calcErsLeft(const vector<double> &x, int index);

    State getInitialState(void);

    OptResult optimise(ObjFunc objFunc, int maxIters, double maxRuntime);

    private:

    double calcInitialEnergy(void);

    double calcObjFunc1(const vector<double> &x);

    void setBounds(nlopt::opt &nlOpt);

    void setConstraints(nlopt::opt &nlOpt);

    vector<double> *genGuess(void);
};


#endif //FEEDBACK_OPTIMISER_HPP
