//
// Created by lachlan on 25/02/17.
//

#include <iostream>
#include "Input.hpp"

// Initialise the static random variables.
std::random_device Input::rdId;
// Initialise the seed.
std::mt19937 Input::genId = std::mt19937(Input::rdId());
// Initialise the distribution.
std::uniform_real_distribution<> Input::randId =
        std::uniform_real_distribution<>(0, INPUT_RESOLUTION + 1);

// Constructor that creates an input using the id and generates the child.
Input::Input(Disturbance *parent, inputId_t &id) : parent(parent), id(id) {
    // Set the input power.
    setInputPower(id);
    // Set the child. Do this after other class members have been constructed.
    child = new Branch(this);
}

// Selects a fringe in the tree. An Input cannot be a fringe as it is
// automatically expanded to a branch upon initialisation.
Node *Input::selectFringe(void) {
    // The only child of this is child so return child's fringe.
    return child->selectFringe();
}

// Set the input power using the id.
void Input::setInputPower(inputId_t id) {
    // Determine the value of the power from the id.
    // Get the range that the limits of the input powers span.
    double range = INPUT_MAX - INPUT_MIN;
    // Get the size of a step to meet the range.
    double step = range / INPUT_RESOLUTION;
    // Calculate the size of the power and set this as the input power.
    inputPower = INPUT_MIN + step * id.val;
}

// Gets the state of the parent branch.
State *Input::getState(void) {
    return parent->getState();
}

// Returns the input power of this.
double Input::getInputPower(void) {
    return inputPower;
}

// Returns the disturbance force that this power is responding to.
double Input::getDisturbanceForce(void) {
    return parent->getDistForce();
}

// Returns the id of this.
inputId_t & Input::getId() {
    return id;
}

// This function shouldn't be called as an Input automatically performs a
// simulation when it is created to move through to child.
void Input::simulate() {
    throw std::runtime_error("Input method 'simulate' called.");
}

// Backpropagates the value of this.
void Input::backPropagate(Node *) {
    // The value of this is the value of child.
    value = child->getValue();
    // Backpropagate the value of this.
    parent->backPropagate(this);
}

// Returns the value of this.
double Input::getValue(void) {
    return value;
}

// Returns the id value of this.
double Input::getIdValue(void) {
    return id.val;
}

// Generates a random input.
Input *Input::genRandInput(Disturbance *parent) {
    // Check whether there is any ers energy left.
    if (parent->getParent()->getState()->j < ERS_LEFT_TOLERANCE) {
        // If there's no ers left then return an input power of almost zero
        // (not zero just to ensure that all the energy is used.
        inputId_t id(1);
        return new Input(parent, id);
    }
    // Generate a random id and use this to make a random instance of this.
    inputId_t id = genRandInputId();
    return new Input(parent, id);
}

// Generates a random input id.
inputId_t Input::genRandInputId(void) {
    inputId_t id((int) randId(genId));
    return id;
}

// Gets the depth of this stage.
int Input::getDepth() {
    return parent->getDepth();
}

// Returns a pointer to the child of this.
Branch *Input::getChild() {
    return child;
}

// Returns whether other is equal to this by id and value.
bool Input::equals(Input *other) {
    return id.equals(other->id) && value == other->value;
}

// Destructor method for Input. This is responsible for releasing any memory
// that was created using 'new' and is stored in this class.
Input::~Input(void) {
    delete child;
}

// Copy constructor. This is used only in Controller for making a copy of the
// trajectory taken.
Input::Input(Input *other, Disturbance *parent) : parent(parent),
          id(other->id), value(other->value), inputPower(other->inputPower) {
    // Make a new branch for the child. This will become the new root.
    child = new Branch(this);
}

// Set the child of this to branch.
void Input::setChild(Branch *branch) {
    child = branch;
}
