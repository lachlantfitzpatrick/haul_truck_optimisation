//
// Created by lachlan on 23/02/17.
//
// Performs an optimisation for a truck according to a series of parameters
// describing both the physical characteristics of the environment the truck
// will be run in and the various characteristics of the optimisation.
//

#include <iostream>
#include <cmath>
#include <limits>
#include "Optimiser.hpp"

using namespace std;

// Wrapper function for running the optimisations.
double optWrapper(const vector<double> &x, vector<double> &jac, void *fData) {
    // Cast the input data to an OptArgs object.
    OptArgs* optArgs = reinterpret_cast<OptArgs*>(fData);
    // Switch on the selected objective function.
    switch (optArgs->objFunc) {
        // Use an objective function.
        case obj1:
            return optArgs->optimiser->objFunc1(x, jac);
        default:
            // Throw an exception if none of the standard objective functions
            // were given.
            throw runtime_error("Failed to recognise objective function.");
    }
}

// A wrapper for the energy used constraints.
double energyUsedWrapper(const vector<double> &x, vector<double> &jac,
                         void* data) {
    // Cast the input data to a ConstraintArgs object.
    Optimiser* optimiser = reinterpret_cast<Optimiser*>(data);
    // Return the result of the constraints function.
    return optimiser->ersLimitConstraint(x, jac, (int) optimiser->getSizeX());
}

// Constructor. Initialises the members.
Optimiser::Optimiser(Parameters params, State initialState) :
        params(params), mass(params.getTotalMass()), initialState(initialState),
        initialEnergy(calcInitialEnergy()) {
    xSize = 0; // Place holding value.
}

// Constructor. Initialises the members.
Optimiser::Optimiser(Parameters params, State initialState, bool) :
        params(params), mass(params.getTotalMass()), initialState(initialState),
        initialEnergy(initialState.j) {
    xSize = 0; // Place holding value.
}

// Calculates the amount of fuel required to produce the inputted energy.
double Optimiser::calcEnergyToFuel(double energy) {
    return energy * fuelEff / fuelDensity;
}

// Calculates the cost of fuel ($AUD) to produce an amount of energy (J).
double Optimiser::calcEnergyToCost(double energy) {
    return calcEnergyToFuel(energy) * fuelCost;
}

// Calculates the initial energy of the truck based on the pit height.
double Optimiser::calcInitialEnergy(void) {
    // 463 is rate of energy recovered per meter of height (Wh/m) which is
    // then converted to Joules ('* 3600').
    return 463 * params.pitHeight * 3600;
}

// Gets the initial state of this.
State Optimiser::getInitialState(void) {
    return initialState;
}

// Performs an optimisation according
OptResult Optimiser::optimise(ObjFunc objFunc, int maxIters) {
    // Try to perform the optimisation.
    try {
        // Uses the overloaded method.
        return optimise(objFunc, maxIters, std::numeric_limits<double>::max());
    } catch (std::runtime_error) {
        // Set the error flag in the nlopt result.
        nlopt::result nloptResult(nlopt::FAILURE);
        // Set all the inputs to zero.
        // Generate a guess.
        vector<double>* xGuess = genGuess();
        // Set the size of the input vector for the optimisation.
        xSize = (unsigned int) xGuess->size();
        // Iterate over all elements of xGuess setting them to zero.
        for (unsigned long i = 0; i < xGuess->size(); i++) {
            xGuess->at(i) = 0;
        }
        // Return the results.
        return OptResult(*xGuess, nloptResult);
    }
}

// Performs an optimisation according
OptResult Optimiser::optimise(ObjFunc objFunc, int maxIters,
                              double maxRuntime) {
    // Generate a guess.
    vector<double>* xGuess = genGuess();
    // Set the size of the input vector for the optimisation.
    xSize = (unsigned int) xGuess->size();

    // Make the optimisation object that will be used to perform the
    // optimisation.
    nlopt::opt nlOpt(nlopt::LD_SLSQP, xSize);

    // Set the constraints.
    setConstraints(nlOpt);
    // Set the bounds.
    setBounds(nlOpt);

    // Setup the arguments of the optimisation.
    OptArgs optArgs(objFunc, this);
    // Use the wrapper as the objective function.
    nlOpt.set_min_objective(optWrapper, &optArgs);

    // Specify the stopping criteria.
    nlOpt.set_maxeval(maxIters);
    nlOpt.set_maxtime(maxRuntime);
    nlOpt.set_xtol_abs(params.tolerance);

    // Try to run the optimisation. This may throw an exception.
    double val; // Variable to fill out the parameters of function optimize.
    nlopt::result nloptResult = nlOpt.optimize(*xGuess, val);
    OptResult *optResult = new OptResult(*xGuess, nloptResult);

    // Return the result.
    return *optResult;
}

// Performs a simulation of the truck using the optimisation variables.
SimResult Optimiser::simulate(const vector<double> &x) {
    // Create a truck that will be simulated.
    Truck truck = Truck(params.slope, initialState, mass, params.muRolling);

    // Perform the simulation. Importantly, the inputs should only change
    // according to the optimisation time step (not the simulation time step).
    int simToOptSteps = (int) floor(params.optTimeStep / params.simTimeStep);
    for (const auto &ersPower : x) {
        for (int j = 0; j < simToOptSteps; j++) {
            // Run a simulation step.
            truck.simulateStep(params.simTimeStep,
                               InputPower(MAX_GEN_POWER, ersPower));
            // Check whether the height has been reached.
            if(truck.getHeight() >= params.pitHeight) {
                // Exit the simulation if the height has been reached after
                // getting the simulation results.
                // Get the simulation results.
                return SimResult(truck.getTime(), truck.getHeight(),
                                    truck.getGenEnergyUsed(),
                                    truck.getErsUsed());
            }
        }
    }

    // Get the simulation results.
    return SimResult(truck.getTime(), truck.getHeight(),
                        truck.getGenEnergyUsed(), truck.getErsUsed());
}

// Given an input vector, determines the objective value of this input. The
// objective is sum of the time ascending the slope and remaining ERS.
double Optimiser::calcObjFunc1(const vector<double> &x) {
    // Simulate a truck responding to the given inputs.
    SimResult simResult = simulate(x);

    // Calculate the penalty to be applied if the truck didn't successfully
    // ascend the slope.
    double penalty = 0;
    if (simResult.height < params.pitHeight) {
        // Scale the difference between the heights by a really large number.
        penalty = 1e18 * (params.pitHeight - simResult.height);
    }

    double res = simResult.time * 1e9 +
                 (initialEnergy - simResult.ersEnergyUsed) * 1e2 + penalty;
    // Calculate and return the objective value.
    return res;
}

// Given an input vector, determines the objective value of this input and
// calculates the gradient.
double Optimiser::objFunc1(const vector<double> &x, vector<double> &jac) {
    // Determine all the gradients.
    // Make copy of x.
    vector<double> xStep(x);
    // Store the objective value for the given input as this won't change.
    double objX = calcObjFunc1(x);
    // Iterate over each dimension of input to get the gradient in each
    // dimension.
    for (int i = 0; i < jac.size(); i++) {
        // Increment the dimension by epsilon.
        xStep[i] += params.epsilon;
        // Use the first difference as the gradient approximation.
        jac[i] = (calcObjFunc1(xStep) - objX) / params.epsilon;
        // Decrement the dimension by epsilon to reset the copy of x.
        xStep[i] -= params.epsilon;
    }
    // Return the objective.
    return calcObjFunc1(x);
}

// Sets the bounds of each input variable.
void Optimiser::setBounds(nlopt::opt &nlOpt) {
    // Set the lower bound to the minimum power that can be supplied.
    nlOpt.set_lower_bounds(MIN_POWER);
    // Set the upper bound to the maximum ers power that can be supplied.
    // IMPORTANT: because only the ers is used as inputs in this code, this
    // maximum is the lesser of the maximum ers power and the maximum ers
    // power that keeps the overall maximum power below the allowed limit.
    double overallErsMax = (MAX_OUT_POWER / OUT_DC_EFF - GEN_DC_EFF * MAX_GEN_POWER)
                           / ERS_DC_EFF;
    (overallErsMax < MAX_ERS_POWER) ? nlOpt.set_upper_bounds(overallErsMax) :
        nlOpt.set_upper_bounds(MAX_ERS_POWER);
}

// Sets the constraints on energy usage.
void Optimiser::setConstraints(nlopt::opt &nlOpt) {
    // Set the constraint for the energy usage.
    nlOpt.add_inequality_constraint(energyUsedWrapper, this, 1e-4);
}

// The constraint function for checking that the amount of ers used doesn't
// exceed the initial ers stored.
double Optimiser::ersLimitConstraint(const vector<double> &x,
                                     vector<double> &jac, int index) {

    // Determine all the gradients.
    // Make copy of x.
    vector<double> xStep(x);
    // Store the value of the ers left for the input as this won't change.
    double ersLeftX = calcErsLeft(x, index);
    // Iterate over each dimension of input to get the gradient in each
    // dimension.
    for (int i = 0; i < jac.size(); i++) {
        // Increment the dimension by epsilon.
        xStep[i] += params.epsilon;
        // Use the first difference as the gradient approximation.
        jac[i] = (calcErsLeft(xStep, index) - ersLeftX) / params.epsilon;
        // Decrement the dimension by epsilon to reset the copy of x.
        xStep[i] -= params.epsilon;
    }

    // Return the constraint value.
    return calcErsLeft(x, index);
}

// Calculates the amount of ers that has been used by the input x up to index.
double Optimiser::calcErsLeft(const vector<double> &x, int index) {
    // Sum the inputs (powers) up to and including the index.
    double sumPowers = 0;
    for (int i = 0; i < index; i++) {
        sumPowers += x[i];
    }

    double res = params.optTimeStep * sumPowers - initialEnergy;
    // Return the energy left that corresponds to the power used.
    return res;
}

// Generates a guess to be used to initialise the optimisation. This guess
// uses ers at the maximum rate. It is imperative that the number of elements
// in this guess is larger than what is required for the truck to ascend the
// slope in the optimisation as the optimiser cannot change the size of the
// input vector if it is unable ascend.
vector<double>* Optimiser::genGuess(void) {
    // Create a truck to be used for the simulation.
    Truck truck = Truck(params.slope, initialState, mass, params.muRolling);
    // Make a local initial energy variable so that the amount of initial
    // energy available to the guess can be controlled.
    double initialEnergy = 0.5 * this->initialEnergy;
    // The max ers power that can be used.
    double ersPowerMax = 0.0 * MAX_ERS_POWER;

    // Run a simulation until the truck has ascended the slope.
    while (truck.getHeight() <= params.pitHeight) {
        // Calculate the ers power to be inputted to the simulation.
        double ersPower = 0;
        double ersUsed = truck.getErsUsed();
        // Check whether using the max ers power will exceed the available
        // energy.
        if (ersUsed + ersPowerMax * params.simTimeStep < initialEnergy) {
            // It is ok to use the max power.
            ersPower = ersPowerMax;
        } else if(ersUsed < initialEnergy) {
            // Use whatever energy is left.
            ersPower = (initialEnergy - ersUsed) / params.simTimeStep;
        }
        // Step the truck forward.
        truck.simulateStep(params.simTimeStep,
                           InputPower(params.guessPower, ersPower));
        // Check that the truck is ascending (and not rolling back down the
        // slope!). Give the truck a chance to do weird stuff initially.
        if (truck.getHistory().size() > 100 &&
                truck.getHistory().rbegin()[0].state.s -
                        truck.getHistory().rbegin()[1].state.s < 0) {
            throw runtime_error("Truck can't ascend slope.");
        }
    }

    // Convert the guess (ers input stored in truck history) from sim step
    // time to opt step time.
    // Get the number of simulation steps in one optimisation step.
    int step = (int) floor(params.optTimeStep / params.simTimeStep);
    // Generate the guess by iterating over the truck's history.
    vector<double> *guess = new vector<double>();
    for (int i = 0; i <= truck.getHistory().size(); i += step) {
        // Append the ers inputted at the index.
        guess->push_back(truck.getHistory()[i].inputPower.ers);
    }
    // Return the guess.
    return guess;
}

// Gets the size of the input vector.
unsigned int Optimiser::getSizeX() {
    return xSize;
}

// Gets the initial energy.
double Optimiser::getInitialEnergy(void) {
    return initialEnergy;
}


