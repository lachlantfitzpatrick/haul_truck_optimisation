//
// Created by lachlan on 25/02/17.
//

#include <iostream>
#include "Disturbance.hpp"

// Initialise the static random variables.
std::random_device Disturbance::rdId;
// Initialise the seed.
std::mt19937 Disturbance::genId = std::mt19937(Disturbance::rdId());
// Initialise the distribution.
std::uniform_real_distribution<> Disturbance::randId =
        std::uniform_real_distribution<>(0, DISTURBANCE_RESOLUTION + 1);

// Constructor. Takes an id and generates the data for the class members.
Disturbance::Disturbance(Branch *parent, distId_t &id) :
        parent(parent), id(id) {
    // Set the disturbance force using the id.
    distForce = genDistForce(id);
    // Initialise the random number generator.
    genStrat = std::mt19937(rdStrat());
    // Distribute the random numbers.
    randStrat = std::uniform_real_distribution<>(0, 1);
}

// Select a fringe from the tree.
Node *Disturbance::selectFringe(void) {
    // If this hasn't been expanded.
    if (!expanded) {
        // Perform the expansion step.
        expanded = true;
        // Return this as the fringe.
        return this;
    }
    // Select the input according to a multiarm bandit strategy.
    // Generate a random number used to select the strategy.
    double rand = randStrat(genStrat);
    // If the random number is below the EXPLORE threshold use an explore
    // strategy or if there are no elements.
    Input *selectedInput = nullptr;
    bool isRandom = false;
    if (rand < EXPLORE || inputMap.empty()) {
        // The explore strategy selects an input randomly.
        selectedInput = Input::genRandInput(this);
        // Indicate the input was randomly generated.
        isRandom = true;
    }
        // Otherwise use the exploit strategy.
    else {
        selectedInput = getExploitedInput();
    }
    // Get the stored input corresponding with the generated input.
    selectedInput = getInput(selectedInput, isRandom);
    // Use the generated input's selection step.
    return selectedInput->selectFringe();
}

// Gets the state associated with the parent Branch.
State *Disturbance::getState() {
    return parent->getState();
}

// Returns the disturbance force of this.
double Disturbance::getDistForce() {
    return distForce;
}

// Generate a random disturbance.
Disturbance *Disturbance::genRandDisturbance(Branch *parent) {
    // Generate the random id used to select the disturbance.
    distId_t id = genRandomDistId();
    return new Disturbance(parent, id);
}

// Returns true if the other Disturbance is equal to this. Equality is
// defined as both Disturbances having the same id and value.
bool Disturbance::equals(Disturbance *other) {
    return id.equals(other->id) && value == other->value;
}

// Returns a copy of the id of this.
distId_t &Disturbance::getId() {
    return id;
}

// Simulate from this until the truck has ascended and record the ascent
// time as the value of this.
void Disturbance::simulate() {
    // Generate a random Input.
    Input *input = Input::genRandInput(this);
    // Create a Stage.
    Stage stage(input, this, parent);
    // Set the value of this using the Controller's simulation. Note that we
    // want the expected ascent time and the controller's simulation only
    // gives the time from where the truck starts.
    value = Controller::simulate(&stage) +
            getDepth() * Controller::getParameters()->optTimeStep;
    // Delete input as it was created with 'new'.
    delete input;
}

// Backpropagates the value of this. If the value of this needs to be
// recalculated this is done as well.
void Disturbance::backPropagate(Node *node) {
    // Check whether there are any children.
    if (inputMap.size() > 0) {
        // Recalculate the value of this.
        // Add the removed input (ie. the node that was passed into this
        // function) back into inputSet.
        Input *removedInput = (Input *) node;
        inputSet.insert(removedInput);
        // The value of this is the lowest value child of this... which is
        // automatically sorted in the set ordering.
        value = (*inputSet.begin())->getValue();
    }
    // The size of map and set should be the same.
    if (inputMap.size() != inputSet.size()) {
        std::cout << "Disturbance bug... set and map aren't same size." << "\n";
    }
    // Backpropagate the value of this.
    parent->backPropagate(this);
}

// Returns the value of this.
double Disturbance::getValue(void) {
    return value;
}

// Uses an 'exploit' strategy to get an input. The exploit strategy tries to
// improve the confidence in the value of higher valued children. This method
// assumes that there are already children.
Input *Disturbance::getExploitedInput() {
    // Generate a random number.
    double rand = randStrat(genStrat);
    // Setup the increment for incrementing the cumulative probability. We
    // want a uniform probability for the first half of the elements in
    // inputSet.
    double size = inputSet.size();
    double increment = 2 / size;
    // Setup the cumulative probability.
    double cumulativeProb = 0;
    // Iterate through the set from start to end.
    for (auto it = inputSet.begin(); it != inputSet.end(); it++) {
        // Increment the cumulative probability.
        cumulativeProb += increment;
        // Check whether the cumulative probability has reached the random
        // number.
        if (cumulativeProb >= rand) {
            return *it;
        }
    }
    // We should never get to here.
    throw std::runtime_error("Did not get exploited input.");
}

// Gets the Input corresponding to input or adds input if it isn't already.
Input *Disturbance::getInput(Input *input, bool isRandom) {
    // Try to get the Input from inputMap.
    try {
        // Get the return input.
        Input *retInput = inputMap.at(input->getId());
        // Remove the return input from the set as it is going to have its
        // values modified. It will be returned to the set during
        // backpropagation.
        inputSet.erase(retInput);
        // Check that set is one element less than map.
        if (inputSet.size() != inputMap.size() - 1) {
            std::cout << "Disturbance bug... set and map aren't same size." << "\n";
        }
        // Check whether given input was generated randomly.
        if (isRandom) {
            // Delete the given input as it is no longer required.
            delete input;
        }
        // Return the input to be returned.
        return retInput;
    } catch (std::out_of_range) {
        // Add the input to the map (but not the set!).
        inputMap.insert(std::pair<inputId_t, Input *>(input->getId(), input));
        return input;
    }
}

// Generates a random force.
double Disturbance::genRandomDistForce() {
    // Generate a random id.
    distId_t id = genRandomDistId();
    // Calculate the size of the force and set this as the disturbance force.
    return genDistForce(id);
}

// Returns the a disturbance force based on the id.
double Disturbance::genDistForce(distId_t id) {
    // Determine the value of the force from the id.
    // Get the range that the limits of the disturbance force span.
    double range = DISTURBANCE_MAX - DISTURBANCE_MIN;
    // Get the size of a step to meet the range.
    double step = range / DISTURBANCE_RESOLUTION;
    // Calculate the size of the force and set this as the disturbance force.
    return DISTURBANCE_MIN + step * id.val;
}

// Generates a random distId_t.
distId_t Disturbance::genRandomDistId() {
    return distId_t((int) randId(genId));
}

// Get the value of the id.
double Disturbance::getIdValue(void) {
    return id.val;
}

// Gets the stage depth of this.
int Disturbance::getDepth(void) {
    return parent->getDepth();
}

// Gets the best input from this.
Input *Disturbance::getBestInput() {
    return *inputSet.begin();
}

// Gets the parent of this.
Branch *Disturbance::getParent() {
    return parent;
}

// Deletes all inputs from the set and map except input.
void Disturbance::cleanup(Input *input) {
    // Erase all elements in the set.
    inputSet.clear();
    // Delete all elements in the map except input.
    for (auto it = inputMap.begin(); it != inputMap.end(); it++) {
        // Check whether this is not input.
        if (!input->equals(it->second)) {
            // Delete the input.
            delete it->second;
            // Repoint the element to the null pointer.
            it->second = nullptr;
        }
    }
    // Clear the map.
    inputMap.clear();
    // Add input back in to map.
    inputMap.insert(std::pair<inputId_t, Input *>(input->getId(), input));
    // Add input back in to set.
    inputSet.insert(input);
}

// Cleans-up all the objects created with the 'new' keyword.
Disturbance::~Disturbance() {
    // Erase all elements in the set.
    inputSet.clear();
    // Remove all elements from the map.
    for (auto it = inputMap.begin(); it != inputMap.end(); it++) {
        // Delete the Disturbance.
        delete it->second;
        // Repoint the element to the null pointer.
        it->second = nullptr;
    }
    // Erase all elements in the map.
    inputMap.clear();
}

// Copy constructor. This is used only in Controller for making a copy of the
// trajectory taken.
Disturbance::Disturbance(Disturbance *other, Branch *parent, Input *child) :
        parent(parent), id(other->id), value(value), expanded(other->expanded) {
    // Set the disturbance force using the id.
    distForce = genDistForce(id);
    // Initialise the random number generator.
    genStrat = std::mt19937(rdStrat());
    // Distribute the random numbers.
    randStrat = std::uniform_real_distribution<>(0, 1);
    // Add the child.
    inputSet.insert(child);
    inputMap.insert(std::pair<inputId_t, Input *>(child->getId(), child));
}
