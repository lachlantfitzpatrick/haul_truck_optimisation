//
// Created by lachlan on 25/02/17.
//

#include <iostream>
#include "Branch.hpp"

// Constructor for the root branch.
Branch::Branch(State state) : isRoot(true), parent(NULL), state(state),
                              depth(0) {}

// Constructor for general branches.
Branch::Branch(Input *parent) : parent(parent) {
    // Set the depth of this to be the increment of parent.
    depth = parent->getDepth() + 1;
    // Set the state of this.
    setState();
}

// Select a fringe from the tree.
Node *Branch::selectFringe() {
    // If this hasn't been expanded or has finished the ascent.
    if (!expanded || finishedAscent) {
        // Perform the expansion step.
        expanded = true;
        // Return this as the fringe.
        return this;
    }
    // Select a disturbance randomly.
    Disturbance *randDist = Disturbance::genRandDisturbance(this);
    // Get the stored disturbance corresponding with the generated disturbance.
    randDist = getDisturbance(randDist);
    // Use the generated disturbance's selection step.
    return randDist->selectFringe();
}

// Gets the state of this.
State *Branch::getState() {
    return &state;
}

// Sets the state of this by simulating a truck from the previous branch to
// this branch.
void Branch::setState() {
    // Simulate a truck from the previous branch to this branch using the
    // inputPower power and disturbance force parents.
    // Get the parameters of the optimisation.
    Parameters *params = Controller::getParameters();
    // Create a truck.
    Truck truck(params->slope, *parent->getState(), params->getTotalMass(),
                params->muRolling);
    // Get the inputPower power.
    double inputPower = getInputPower();
    // Get the disturbance force.
    double distForce = getDisturbanceForce();
    // Simulate the truck through the initial powers.
    Controller::simulateOptStep(&truck, inputPower, distForce);
    // Check whether the brim was reached.
    if (truck.getHeight() >= params->pitHeight) {
        // Record the time of ascent. Note that 1 must be subtracted from the
        // depth because it is only the time up to the start of this stage
        // that needs to be included (this simulation is for determining the
        // initial state of this NOT the final).
        ascentTime = truck.getTime() + params->optTimeStep * (depth - 1);
        // Set this branch as finished. It is not the responsibility of this
        // function to backpropagate though.
        finishedAscent = true;
    }
    // Set the state of the branch as the final state that the truck reached.
    state = truck.getState();
}

// Gets the input power used to get from the last branch to this.
double Branch::getInputPower() {
    return parent->getInputPower();
}

// Gets the disturbance force used to get from the last branch to this.
double Branch::getDisturbanceForce() {
    return parent->getDisturbanceForce();
}

// Gets the Disturbance corresponding to dist or adds dist if it isn't already.
Disturbance *Branch::getDisturbance(Disturbance *dist) {
    // Try to get the Disturbance from disturbances.
    try {
        // Try and get the disturbance from the map.
        Disturbance *mappedDist = disturbances.at(dist->getId());
        // If we got it then we want to delete the given disturbance as it is
        // no longer required.
        delete dist;
        // Return the mapped disturbance.
        return mappedDist;
    }
        // Catch the key out of range exception indicating that dist wasn't found.
    catch (std::out_of_range) {
        // Add the disturbance to the map.
        disturbances.insert(std::pair<distId_t, Disturbance *>
                                    (dist->getId(), dist));
        // Return dist.
        return dist;
    }
}

// Simulate from this until the truck has ascended and record the ascent
// time as the value of this.
void Branch::simulate() {
    // Generate a random Disturbance.
    Disturbance *dist = Disturbance::genRandDisturbance(this);
    // Generate a random Input.
    Input *input = Input::genRandInput(dist);
    // Make a Stage.
    Stage stage(input, dist, this);
    // Set the value of this using the Controller's simulation. Note that we
    // want the expected ascent time and the controller's simulation only
    // gives the time from where the truck starts.
    value = Controller::simulate(&stage) +
            depth * Controller::getParameters()->optTimeStep;
    // Delete the created input and disturbance as these were created with
    // 'new'.
    delete dist; delete input;
}

// Backpropagates the value of this. If this is root then it does not need to
// backpropagate. If the value of this needs to be recalculated performs
// this as well.
void Branch::backPropagate(Node *) {
    // Do nothing if it is root.
    if (isRoot) return;
    // Check whether there are any children.
    if (disturbances.size() > 0) {
        // Recalculate the value of this.
        // Iterate over the children to get the average value.
        double sum = 0;
        for (auto it = disturbances.begin(); it != disturbances.end(); it++) {
            // Add the value of the disturbance at it to the sum.
            sum += it->second->getValue();
        }
        // Set the value to the average.
        value = sum / ((double) disturbances.size());
    }
    // Call the parent's backpropagate method so that it can be updated using
    // the new value of this.
    parent->backPropagate(this);
}

// Gets the value of this.
double Branch::getValue(void) {
    return value;
}

// Gets the value of id of this (which is actually just the parent input's id).
double Branch::getIdValue(void) {
    // If root then return the lowest number possible.
    if (isRoot) {
        return std::numeric_limits<double>().lowest();
    }
    return parent->getIdValue();
}

// Gets the depth of this.
int Branch::getDepth() {
    return depth;
}

// Returns true if this has reached the brim of the pit or false otherwise.
bool Branch::isFinished() {
    return finishedAscent;
}

// Given dist, gets the best input response to it.
Input *Branch::getBestInput(Disturbance *dist) {
    // Try to get the disturbance.
    try {
        // Get the disturbance corresponding to dist from the map.
        dist = disturbances.at(dist->getId());
        // Get the best input from this.
        return dist->getBestInput();
    }
        // Catch the key out of range exception indicating that dist wasn't found.
    catch (std::out_of_range) {
        // Print to indicate that we failed getting the disturbance.
        std::cout << "\n" << "!!Didn't find the disturbance!!" << "\n" << "\n";
        // Use the median input.
        inputId_t id(INPUT_RESOLUTION / 2);
        return new Input(dist, id);
    }
}

// Randomly selects a disturbance from the disturbances. Assumes that at
// least one disturbance exists.
Disturbance *Branch::getRandomDisturbance() {
    // Create a distribution of random numbers.
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> rand(0, disturbances.size());
    // Get an iterator over disturbances.
    auto it = disturbances.begin();
    std::advance(it, (int) rand(gen));
    return it->second;
}

// Set this as root.
void Branch::setAsRoot() {
    isRoot = true;
}

// Gets the time that this finished.
double Branch::getTime() {
    if (finishedAscent) {
        return ascentTime;
    } else {
        return -1;
    }
}

// Deletes all disturbances from the map except dist and then performs the
// same process with dist on input.
void Branch::cleanup(Disturbance *dist, Input *input) {
    // Delete all elements in disturbances.
    for (auto it = disturbances.begin(); it != disturbances.end(); it++) {
        // Check whether this element is dist.
        if (!it->second->equals(dist)) {
            // Delete the Disturbance.
            delete it->second;
            // Repoint the element to the null pointer.
            it->second = nullptr;
        }
    }
    // Clear the map.
    disturbances.clear();
    // Add dist back in.
    disturbances.insert(std::pair<distId_t, Disturbance *>
                                (dist->getId(), dist));
    // Cleanup dist.
    dist->cleanup(input);
}

// Delete all objects that were created with the 'new' keyword.
Branch::~Branch(void) {
    // Delete all elements from the map.
    for (auto it = disturbances.begin(); it != disturbances.end(); it++) {
        // Delete the Disturbance.
        delete it->second;
        // Repoint the element to the null pointer.
        it->second = nullptr;
    }
    // Erase all elements in the map.
    disturbances.clear();
}

// Copy constructor. This is used only in Controller for making a copy of the
// trajectory taken.
Branch::Branch(Branch *other, Input *parent, Disturbance *child) :
        parent(parent), depth(other->depth), value(other->value),
        isRoot(other->isRoot), expanded(other->expanded),
        finishedAscent(other->finishedAscent), state(other->state) {
    // Add the child.
    disturbances.insert(std::pair<distId_t, Disturbance *>
                                (child->getId(), child));
    // Check whether this is grandfather root.
    if (parent != nullptr) {
        // Add this as a child to parent.
        parent->setChild(this);
    }
}
