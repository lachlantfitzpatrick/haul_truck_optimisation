//
// Created by lachlan on 25/02/17.
//
// A branch represents a state of the tree after a Disturbance and Input has
// been made.
//

#ifndef FEEDBACK_BRANCH_HPP
#define FEEDBACK_BRANCH_HPP

#include <map>
#include "Node.hpp"
#include "Input.hpp"
#include "Disturbance.hpp"
#include "Defines.hpp"
#include "Optimiser.hpp"
#include "Controller.hpp"

class Input;

class Disturbance;

class Node;

class Branch : public Node {

    Input *parent;

    std::map<distId_t, Disturbance *, CompareDistIds> disturbances;

    double value;
    int depth;
    double ascentTime;

    bool isRoot = false;
    bool expanded = false;
    bool finishedAscent = false;

    State state;

public:

    Branch(State state);

    Branch(Input *);

    Branch(Branch *, Input *, Disturbance *);

    Node *selectFringe(void) override;

    void simulate(void) override;

    void backPropagate(Node *) override;

    double getValue(void) override;

    State *getState(void) override;

    double getIdValue(void) override;

    int getDepth() override;

    bool isFinished();

    double getTime();

    void cleanup(Disturbance *, Input *);

    void setAsRoot();

    Input *getBestInput(Disturbance *);

    Disturbance *getRandomDisturbance();

    ~Branch(void);

private:

    Disturbance *getDisturbance(Disturbance *);

    void setState(void);

    double getInputPower(void);

    double getDisturbanceForce(void);
};


#endif //FEEDBACK_BRANCH_HPP
