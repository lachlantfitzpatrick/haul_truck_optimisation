//
// Created by lachlan on 25/02/17.
//

#ifndef FEEDBACK_DISTURBANCE_HPP
#define FEEDBACK_DISTURBANCE_HPP

#include <set>
#include <map>
#include <random>
#include <cmath>
#include "Node.hpp"
#include "Input.hpp"
#include "Branch.hpp"
#include "BoundedInteger.hpp"
#include "Defines.hpp"
#include "Optimiser.hpp"
#include "Controller.hpp"

class Branch;

class Input;

class Disturbance : public Node {

    Branch *parent;
    distId_t id;

    std::set<Input *, CompareNodeValue> inputSet;
    std::map<inputId_t, Input *, CompareInputIds> inputMap;

    double value;
    double distForce;
    bool expanded = false;

    static std::random_device rdId;
    static std::mt19937 genId;
    static std::uniform_real_distribution<> randId;
    std::random_device rdStrat;
    std::mt19937 genStrat;
    std::uniform_real_distribution<> randStrat;

public:

    Disturbance(Branch *, distId_t &);

    Disturbance(Disturbance *, Branch *, Input *);

    Node *selectFringe(void) override;

    void simulate(void) override;

    void backPropagate(Node *) override;

    double getValue(void) override;

    State *getState(void) override;

    double getIdValue(void) override;

    int getDepth(void) override;

    bool equals(Disturbance *);

    double getDistForce(void);

    distId_t & getId();

    Input *getBestInput();

    void cleanup(Input *pInput);

    // Temporary for testing only.
    void setValue(double newVal)  {value = newVal;}

    Branch *getParent();

    static Disturbance * genRandDisturbance(Branch *);

    static double genRandomDistForce();

    static double genDistForce(distId_t);

    static distId_t genRandomDistId(void);

    ~Disturbance(void);

private:

    Input *getExploitedInput();

    Input *getInput(Input *, bool);
};


#endif //FEEDBACK_DISTURBANCE_HPP
