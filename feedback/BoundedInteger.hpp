//
// Created by lachlan on 26/02/17.
//

#ifndef FEEDBACK_BOUNDEDINTEGER_HPP
#define FEEDBACK_BOUNDEDINTEGER_HPP

#include <assert.h>

template<int min, int max> struct BoundedInteger {

    int val;
    // Constructor. Initialises val to given val.
    BoundedInteger(int val) {
        // Assert that val is between min and max (inclusive).
        assert(val >= min);
        assert(val <= max);
        // If it was between the limits then set the value.
        this->val = val;
    }

    // Check whether two bounded integers are equal. Equality is based only
    // on the value not the limits.
    bool equals(const BoundedInteger &other)const {
        return this->val == other.val;
    }
};

#endif //FEEDBACK_BOUNDEDINTEGER_HPP
