//
// Created by lachlan on 25/02/17.
//

#ifndef FEEDBACK_INPUT_HPP
#define FEEDBACK_INPUT_HPP

#include "Node.hpp"
#include "Branch.hpp"
#include "Disturbance.hpp"
#include "Defines.hpp"
#include "Optimiser.hpp"
#include "Controller.hpp"

class Branch;

class Disturbance;

class Input : public Node {

    Disturbance *parent;
    Branch *child;
    inputId_t id;

    double value;
    double inputPower;

    static std::random_device rdId;
    static std::mt19937 genId;
    static std::uniform_real_distribution<> randId;

public:

    Input(Disturbance *, inputId_t &);

    Input(Input *, Disturbance *);

    Node *selectFringe(void) override;

    void simulate(void) override;

    void backPropagate(Node *) override;

    double getValue(void) override;

    State *getState(void) override;

    double getIdValue(void) override;

    int getDepth() override;

    double getDisturbanceForce(void);

    double getInputPower(void);

    bool equals(Input *);

    inputId_t &getId();

    Branch *getChild();

    void setChild(Branch *);

    static Input * genRandInput(Disturbance *parent);

    static inputId_t genRandInputId(void);

    ~Input(void);

private:

    void setInputPower(inputId_t id);
};


#endif //FEEDBACK_INPUT_HPP
