#!/usr/bin/env gnuplot

# Variables
dataFolder = "cmake-build-debug/"

# Settings
load "settings.gnu"

# Output
set terminal svg size 1000,500 font "Helvetica Neue Light,16"
set output "plot.input-disturbance-time.svg"

# Key
set key outside center bottom horizontal

# Labels
set title "Input and Disturbance Forces applied over Open Loop and Closed Loop Control"
set xlabel "Time (s)"
set ylabel "Input Power (W)"
set y2label "Disturbance Force (N)"

# Axes
set tics in
set ytics nomirror
set y2tics
set grid xtics

# Plot
set multiplot
plot sprintf("%s%s", dataFolder, "feedback.csv") using 1:3 with lines ls 2 title "Feedback Inputs", \
     sprintf("%s%s", dataFolder, "openloop.csv") using 1:3 with lines ls 3 title "Open Loop Inputs", \
     sprintf("%s%s", dataFolder, "optfeedback.csv") using 1:3 with lines ls 4 title "Optimised Feedback Inputs", \
     sprintf("%s%s", dataFolder, "feedback.csv") using 1:2 axes x1y2 with lines ls 1 title "Disturbances", \