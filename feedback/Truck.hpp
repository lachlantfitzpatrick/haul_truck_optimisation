//
// Created by lachlan on 8/02/17.
//

#ifndef FEEDBACK_TRUCK_H
#define FEEDBACK_TRUCK_H

#include "Slope.hpp"
#include <vector>

// Physical properties of the power flow in the truck.
#define GEN_DC_EFF              0.95
#define ERS_DC_EFF              0.9
#define OUT_DC_EFF              0.85
#define MAX_ERS_POWER           1390000
#define MAX_GEN_POWER           2390000
#define MAX_OUT_POWER           2800000
#define MIN_POWER               0

// A state of the truck.
typedef struct State {
    // The default values are the default initial values for a truck about to
    // ascend.
    double s = 0.0; // Distance along slope.
    double v = 7.0; // Speed along slope.
    double j = 0.0; // Stored energy in ERS.

    State() {}

    State(const State &copy) : s(copy.s), v(copy.v), j(copy.j) {}

} State;

// An input power for the truck.
typedef struct InputPower {
    double ers;
    double gen;

    InputPower(double gen, double ers) {
        this->gen = gen;
        this->ers = ers;
    }
} InputPower;

// A instant in the history of a truck.
typedef struct Step {
    double time;
    State state;
    InputPower inputPower;
    double ersUsed;
    double genUsed;

    Step(double time, State state, InputPower inputPower, double ersUsed,
         double genUsed) : time(time), state(state), inputPower(inputPower),
                           ersUsed(ersUsed), genUsed(genUsed) {}
} Step;

// The truck class.
class Truck {

    //// Constants
    // Value of gravity.
    const double g = 9.807;
    // Current state of the truck.
    State state;
    // Total mass of the truck (product and chassis).
    double mass;
    // Coefficient of friction between wheels and ground.
    double muRolling;
    // The time that the truck has been running for.
    double time;
    // The array of all the historical steps and states.
    std::vector<Step> history;
    // The slope the truck is trying to ascend.
    Slope slope;

    //// Functions
public:
    Truck(Slope slope, State initialState, double mass, double muRolling);

    void simulateStep(double timeStep, InputPower inputPower);

    void simulateStep(double timeStep, InputPower inputPower,
                      double disturbanceForce);

    const double getGenEnergyUsed();

    const double getErsUsed();

    State getState();

    double getTime();

    const double getHeight();

    const vector<Step> getHistory();

    double getErsLeft();

private:
    double calcNetForce(double tractionF, double gravityF, double frictionF,
                        double disturbanceF);

    double calcTractionForce(double appliedPower);

    double calcSaturationVelocity(double appliedPower, double m, double c);

    void applyPowerLimits(InputPower &inputPower);

    double calcAppliedPower(const InputPower &inputPower);
};


#endif //FEEDBACK_TRUCK_H
