//
// Created by lachlan on 25/02/17.
//
// This is responsible for controlling the truck's trajectory up a slope
// using an mcts algorithm.
//

#ifndef FEEDBACK_CONTROLLER_HPP
#define FEEDBACK_CONTROLLER_HPP

#include "Optimiser.hpp"
#include "Input.hpp"
#include "Disturbance.hpp"
#include "Branch.hpp"
#include "Truck.hpp"

class Input;

class Disturbance;

class Branch;

// A stage consists of a Branch and a child Disturbance and further child Input.
typedef struct Stage {
    Input *input;
    Disturbance *dist;
    Branch *branch;

    Stage(Input *input, Disturbance *disturbance, Branch *branch) :
            input(input), dist(disturbance), branch(branch) {}
} Stage;

typedef struct ControlResult {
    const double time;
    const double height;
    const double genEnergyUsed;
    const double ersLeft;
    const std::vector<double> *inputs;
    const std::vector<double> *dists;
    Truck *truck;

    ControlResult(double time, double height, double genEnergyUsed,
              double ersEnergyLeft, std::vector<double> *inputs,
                  std::vector<double> *dists, Truck *truck) :
            time(time), height(height), genEnergyUsed(genEnergyUsed),
            ersLeft(ersEnergyLeft), inputs(inputs), dists(dists),
            truck(truck) {}
} ControlResult;

class Controller {

    // The parameters of the optimisation.
    static Parameters *params;
    // The results of the optimisation.
    static OptResult *optResult;
    // The initial state of the optimisation.
    const State &initialState;

    // History of what has occurred thus far.
    std::vector<Stage *> history;

    // Root of the tree that is currently being explored.
    Branch *root;

public:
    Controller(Parameters *params, State &initialState, OptResult *optResult);

    void run();

    void plot();

    void save(ControlResult *, std::string);

    void printHistory();

    void printEdgeInformation();

    ControlResult *simulateFeedback();

    ControlResult *simulateOpenloop();

    ControlResult *runOptimisationFeedback();

    static Parameters *getParameters();

    static double simulate(Stage *);

    static void simulateOptStep(Truck *, double inputPower, double distForce);

    static void printResults(ControlResult *);

private:
    void runMCTS();

    void printStage(int);

    Stage *makeNextStage(Branch *, Disturbance *, Input *);

    ControlResult *
    simulateOptFeedback(vector<double> *, vector<double> *);
};


#endif //FEEDBACK_CONTROLLER_HPP
