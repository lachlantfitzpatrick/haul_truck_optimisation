//
// Created by lachlan on 9/02/17.
//

#include "Slope.hpp"
#include <cmath>

using namespace std;

// Constructor. Sets gradients and determines angles.
Slope::Slope(map<double, double> gradients) : gradients(gradients){
    angles = gradsToRads(&gradients);
}

// Takes a map of gradients and iterates through it determining the angle
// associated with each gradient in radians.
map<double, double> Slope::gradsToRads(const map<double, double> *gradients) {
    // Output data.
    map<double, double> angles;

    // Iterate over all the gradients.
    for (const auto &grad : *gradients) {
        angles[grad.first] = atan(grad.second);
    }

    return angles;
}

// Get the angle at some distance s. Assumes that s lies in R++.
double Slope::getAngle(double s) {
    // If there is only one angle.
    if (angles.size() == 1) {
        // Return the first angle.
        return angles.begin()->second;
    }
    // Iterate over all the elements in angles.
    double last = angles.begin()->first;
    for (const auto &angle : angles) {
        // Check whether distance s is between the last distance and next
        // distance.
        if(s >= last && s < angle.first) {
            return angles[last];
        }
        // Update last to the current distance.
        last = angle.first;
    }
    // Distance did not lie between any distances therefore must be after the
    // last element (or before the first if a -ve s was passed in).
    return angles.rbegin()->second;
}

// Get the gradient at some distance s. Assumes that s lies in R++.
double Slope::getGradient(double s) {
    // If there is only one gradient.
    if (gradients.size() == 1) {
        // Return the first gradient.
        return gradients.begin()->second;
    }
    // Iterate over all the elements in gradients.
    double last = gradients.begin()->first;
    for (const auto &grad : gradients) {
        // Check whether distance s is between the last distance and next
        // distance.
        if(s >= last && s < grad.first) {
            return gradients[last];
        }
        // Update last to the current distance.
        last = grad.first;
    }
    // Distance did not lie between any distances therefore must be after the
    // last element (or before the first if a -ve s was passed in).
    return gradients.rbegin()->second;
}

// Determines the height at some distance. Assumes that s lies on R++.
double Slope::getHeight(double s) {
    // If there is only one angle.
    if (angles.size() == 1) {
        // Calculate the height using only this angle.
        return s * sin(angles.begin()->second);
    }
    // Iterate over all the elements in angles accumulating height.
    double height = 0;
    double last = angles.begin()->first;
    for (const auto &angle : angles) {
        // Check whether distance s is before the next distance.
        if(s < angle.first) {
            // Add the remaining height on and return it.
            return height + (s - last)*sin(angles[last]);
        } else {
            // Add the height on and continue iterating.
            height += (angle.first - last) * sin(angles[last]);
        }
        // Update last to the current distance.
        last = angle.first;
    }
    // Distance did not lie between any distances therefore must be after the
    // last element (or before the first if a -ve s was passed in).
    return height + (s-last)*sin(angles[last]);
}
