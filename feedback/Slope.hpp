//
// Created by lachlan on 9/02/17.
//

#ifndef FEEDBACK_SLOPE_H
#define FEEDBACK_SLOPE_H

#include <vector>
#include <map>

using namespace std;

class Slope {
    map<double, double> gradients;
    map<double, double> angles;

public:
    Slope(map<double, double> gradients);

    map<double, double> gradsToRads(const map<double, double> *gradients);

    double getAngle(double s);

    double getGradient(double s);

    double getHeight(double s);
};

#endif //FEEDBACK_TRUCK_H