#!/usr/bin/env gnuplot

# Variables
dataFolder = "cmake-build-debug/"

# Settings
load "settings.gnu"

# Output
set terminal svg size 1000,500 font "Helvetica Neue Light,16"
set output "plot.distance-time.svg"

# Key
set key outside center bottom horizontal

# Labels
set title "Input and Disturbance Forces applied over Open Loop and Closed Loop Control"
set xlabel "Time (s)"
set ylabel "Distance (m)"

# Axes
set tics in
set ytics nomirror
set grid xtics

# Plot
set multiplot
plot sprintf("%s%s", dataFolder, "feedback.csv") using 1:4 with lines ls 2 title "Feedback", \
     sprintf("%s%s", dataFolder, "openloop.csv") using 1:4 with lines ls 3 title "Open Loop", \