//
// Created by lachlan on 25/02/17.
//
// Interface for a node in a tree that forces the implementation of key
// methods.
//

#ifndef FEEDBACK_NODE_HPP
#define FEEDBACK_NODE_HPP

#include "Truck.hpp"
#include "Defines.hpp"


class Node {

public:
    virtual Node *selectFringe(void) = 0;

    virtual void simulate(void) = 0;

    virtual void backPropagate(Node *) = 0;

    virtual double getValue(void) = 0;

    virtual State *getState(void) = 0;

    virtual double getIdValue(void) = 0;

    virtual int getDepth(void) = 0;
};


// Custom comparator for ordering such that a lower value is ordered first
// and if values are the same, a lower id is order lower.
struct CompareNodeValue {
    bool operator()(Node *n1, Node *n2) const {
        if (n1->getValue() == n2->getValue()) {
            return n1->getIdValue() < n2->getIdValue();
        }
        return n1->getValue() < n2->getValue();
    }
};

// Custom comparator for checking if two disturbances are equal (which is
// done according to their ids).
struct CompareDistIds {
    bool operator()(const distId_t &id1, const distId_t &id2) const {
        return id1.val < id2.val;
    }
};

// Custom comparator for checking if two inputs have the same id.
struct CompareInputIds {
    bool operator()(const inputId_t &id1, const inputId_t &id2) const {
        return id1.val < id2.val;
    }
};

#endif //FEEDBACK_NODE_HPP
